<?php
require('core/ini.php');
helper::loadHeader('header.php',array(
		'TITLE'=>"Solutions"
		,'DESCRIPTION'=>"Liste des solutions enregistrées"));
//si on arrive de 'trouver une solution existante', enregistrer l'id du problème pour plus tard
if(null !== Input::get('problemId') && is_numeric(Input::get('problemId'))){
	Session::put('problemId',Input::get('problemId'));
}
$form = new form();
$categories = helper::getCategories();
$filters = '';
$sortOptions[0] = (object) array('id' => '1', 'name' => $_SESSION['words'][221]);
$sortOptions[1] = (object) array('id' => '2', 'name' => $_SESSION['words'][85]);
$sortOptions[2] = (object) array('id' => '3', 'name' => $_SESSION['words'][101]);
$filters .=  $form->createField('select','sort',98,'',$sortOptions);
$filters .=  $form->createField('select','category',85,'',$categories);
$filters .=  $form->createField('text','searchTerms',2,'');
$filters .=  $form->createField('button','Search',$_SESSION['words'][72],'',"searchAJAX($('#itemList').data('type'))");
?>
<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid">
<div itemprop="description" style="display: none;"><?php echo $_SESSION['words'][220]?></div>
	<div class="row">
		<div class="w-100 d-flex flex-wrap faded_gray_bkgd p-2">
						<h3 class="w-100"><?php echo $_SESSION['words'][255]?></h3><!-- solutions -->
	<?php
	echo $filters;
	?>
		</div>
		<div class="w-100 d-flex justify-content-md-around m-1">
			<div id='start' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
			<div id='previous' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
			<div id='current' class="pages font_green" data-nb="0"></div>
			<div id='next' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
			<div id='end' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
		</div>
	</div>
	<div class="row list" id="itemList" data-type="solution">
	</div>
</div>





<?php
include("inc/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {

	});
$(".pages").show();
</script>
