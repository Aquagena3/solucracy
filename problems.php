<?php
 require('core/ini.php');


helper::loadHeader('header.php',array(
		'TITLE'=>$_SESSION['words'][161]
		,'DESCRIPTION'=>$_SESSION['words'][231]));
//where to get the markers
$getProblemList = 'problemmarkers.php';
$categories = helper::getCategories();
$form = new form();
$user = new user();
if($user->isLoggedIn()){
	$groups = $user->getCommunities();
}
$tag = "";
if(Input::defined('tag')){
	$tag = Input::get('tag');
}
$scopeList = helper::scope();
$filters = '';
$status[0] = (object) array('id' => '1', 'name' => $_SESSION['words'][268]);
$status[1] = (object) array('id' => '3', 'name' => $_SESSION['words'][269]);
$status[2] = (object) array('id' => '4', 'name' => $_SESSION['words'][270]);
$filters .=  $form->createField('select','status',97,'',$status);
$filters .=  $form->createField('swap','solutionSearch',470,'');
$sortOptions[0] = (object) array('id' => '1', 'name' => $_SESSION['words'][99]);
$sortOptions[1] = (object) array('id' => '2', 'name' => $_SESSION['words'][85]);
$sortOptions[2] = (object) array('id' => '3', 'name' => $_SESSION['words'][101]);
$sortOptions[4] = (object) array('id' => '4', 'name' => $_SESSION['words'][109]);
$filters .=  $form->createField('select','sort',98,'',$sortOptions);
$filters .=  $form->createField('select','category',85,'',$categories);
$filters .= $form->createField('select','scopeId',67,67,$scopeList);
if($user->isLoggedIn() && count($groups)>0){
	$filters .=  $form->createField('select','group',392,'',$groups);
}
$filters .=  $form->createField('text','topic_search',2,'',$tag);
$filters .=  $form->createField('button','Search',$_SESSION['words'][72],'',"searchAJAX($('#itemList').data('type'))");
?>
	<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid full-height marginbt100 m-0">
	<div itemprop="description" style="display: none;">Nous avons tous les mêmes problèmes, trouvons ensemble les solutions</div>
		<div class="row full-height">
			<div itemscope itemtype="http://schema.org/ItemPage" class="col-md-6">
				<div class="row">
					<div class="w-100 d-flex flex-wrap faded_gray_bkgd p-2">
						<h3 class="w-100"><?php echo $_SESSION['words'][161]?></h3><!-- problems -->
	<?php
	echo $filters;
	?>
					</div>
					<div class="w-100 d-flex justify-content-md-around">
							<div id='start' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='previous' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='current' class="pages font_green" data-nb="0"></div>
							<div id='next' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='end' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
					</div>
					<div id="itemList" class="row w-100 list p-3" data-type="problem">
					</div>
				</div>
			</div>
			<div class="col-md-6 col-12 full-height m-0 pl-0">
				<div id="mapContainerShow" class="full-height" style="position:relative">
					<div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo Config::get('GMap_API_Key') ?>" width="WIDTH" height="HEIGHT" ></div>
				</div>
			</div>
		</div>
	</div>
<ul id="tlyPageGuide" data-tourtitle="Comment ça marche?">
	<li class="tlypageguide_left" data-tourtarget=".loginButton">
		<div>
			Cliquez ici pour vous connecter, créer un compte ou vous déconnecter.
		</div>
	</li>
	<li class="tlypageguide_left" data-tourtarget="#categories">
		<div>
			Sélectionnez les catégories pour filtrer la liste de problèmes.
		</div>
	</li>
	<li class="tlypageguide_right" data-tourtarget="#search_bar">
		<div>
			Ou saisissez un mot clé.
		</div>
	</li>
	<li class="tlypageguide_right" data-tourtarget="#topics">
		<div>
			Cliquez sur un problème pour afficher sa page.
		</div>
	</li>
	<li class="tlypageguide_right" data-tourtarget="#menu">
		<div>
			Ou utilisez le menu pour naviguer sur d'autres pages.
		</div>
	</li>
</ul>
<?php
include("inc/footer.php");
?>
<script type="text/javascript">
// $(document).ready(function() {
// searchAJAX($('#itemList').data('type'));
// 	});
(tarteaucitron.job = tarteaucitron.job || []).push('googlemapssearch');
</script>
