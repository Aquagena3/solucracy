<?php
require('core/ini.php');
//Creer un objet problem
$problem = new problem(Input::get('id'));
//Récupérer ses données
$problemDetails = $problem->data();
//Couleurs acceptables pour le schéma
$colors = ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928'];
//Position du problème
$location = $problemDetails->latitude.",".$problemDetails->longitude;
//Si le problème a été créé moins de 2 semaines avant, laisser à l'auteur le droit de modifier le titre
$editTitle = false;
$nbDays = strtotime(date('Y-m-d'))-strtotime($problemDetails->createdOn);
$timePassed = floor($nbDays/(60*60*24));
$timeLeft = 14 - $timePassed;
if ($problemDetails->userId === Session::get(Config::get('session/session_name')) && $timePassed < 14) {
	$editTitle = true;
}

//Charger le header pour la page
helper::loadHeader('header.php',array(
	'TITLE'=>$problemDetails->title
	,'DESCRIPTION'=>$problemDetails->description));

//attraper les données de la communauté responsable de ce problème
$community = $problem->currentOwner();
$contactProblemUsers = "";
$notMyProblem = "";
$handledBy = "";
//Si l'utilisateur est connecté en tant que communauté et que ce problème est lié à la communauté
if(Session::exists('communityAdmin') && $community !== false && $community->communityId === Session::get('communityAdmin')){
$contactProblemUsers = "<div onclick='ajax(\"buildform.php\",{type:\"contactProblemUsers\",problemId:".$problemDetails->problemId."},\"form\")' class='clickable'>".$_SESSION['words'][413]." <i class='fa fa-warning'></i></div> <!-- contact users impacted -->";
$notMyProblem = "<div onclick='ajax(\"buildform.php\",{type:\"notMyProblem\",problemId:".$problemDetails->problemId."},\"form\")' class='clickable'>".$_SESSION['words'][418]." <i class='fa fa-warning'></i></div> <!-- Déléguer ce problème -->";
}
if($problemDetails->communityName !== NULL ){
	$handledBy = '<span class="font-weight-light col-12 text-left"">'.$_SESSION['words'][471].'<a class="font-weight-bold" href="communityprofile.php?communityId='.$problemDetails->communityId.'"><u>'.$problemDetails->communityName.'</u></a></span>';//this problem is currently handled by
}
	$displayPropositions = "</br>".$_SESSION['words'][232].".";//No solution has been logged yet
//récupérer les facettes du problème
	$facets = $problem->getFacets();
	$k=0;
	$displayFacets = '';
	foreach ($facets as $facet) {
		$newFacets[$facet->facetId] = $facet;
		$newFacets[$facet->facetId]->order = $k;
		$displayFacets .= ',["'.$facet->description.'",'.$facet->nbVotes.']';
		$k++;
	}
//s'il y a au moins une solution proposée pour ce problème
if($problemDetails->nbSolutions > 0){
	$displayPropositions = "";
	$canVote = false;
	//définir si elles ont eu des votes positifs et l'afficher
	$positiveVotes = $problemDetails->solutionsPertinence;
	foreach ($positiveVotes as $positiveVote) {
		//calcul pertinence : nombre de votes positifs divisé par le nombre total de votes pour cette facette
		$pertinence = round(($positiveVote->nbVotes/$positiveVote->facetVotes)*100,2);
		//filtrer uniquement les facettes que cette proposition adresse
		$facetIds = explode(',',$positiveVote->facetIds);
		$displayPropositions .= '<div class="blackBorder margin5 padding5">';
		$i = 0;
		$barGraph = "";
		foreach ($facetIds as $facetId) {
			//calculer la taille de la barre sur le graphe en fonction du nombre de votes
			$size = round(($newFacets[$facetId]->nbVotes/$problemDetails->nbVotes)*100);
			$barGraph .= '<div style="min-width: 30%;height: '.$size.'px; background-color: '.$colors[$newFacets[$facetId]->order].'; display: inline-block;" data-toggle="tooltip" title="'.$newFacets[$facetId]->description.'">&nbsp;</div>';
			//si l'utilisateur a voté pour l'une de ces facettes, l'autoriser à évaluer la proposition
			if($newFacets[$facetId]->voted > 0){
				$canVote = true;
			}
			$i++;
		}
		$buttons = "";
		//pas soutenu par communauté, admin non connecté
		$ribbon = "";
		if(Session::exists('communityAdmin') && $community !== false && $community->communityId === Session::get('communityAdmin')){
			//pas soutenu par communauté, admin connecté
			if(empty($positiveVote->endorsed)){
				$ribbon = '<div class="col-1 p-1"><i class="fa fa-2x fa-plus font_green clickable" onclick="ajax(\'buildform.php\',{type:\'supportProposition\',propositionId:'.$positiveVote->propositionId.'},\'form\')" data-toggle="tooltip" data-placement="top" title="'.$_SESSION['words'][423].'"></i></div>';
				//soutenu par communauté, admin connecté
			}elseif(!empty($positiveVote->endorsed)){
				$ribbon = '<div class="col-1"><img src="img/support.png" height="64" width="64" class="clickable font_green float-right mt-0 mr-0" onclick="stopSupport('.$positiveVote->propositionId.')" data-toggle="tooltip" data-placement="top" title="'.$_SESSION['words'][425].' '.$positiveVote->comment.'"></div>';
			}
			//soutenu par communauté, admin non connecté
		} elseif(!empty($positiveVote->endorsed)){
			$ribbon = '<div class="col-1"><img src="img/support.png" height="64" width="64" data-toggle="tooltip" data-placement="top" title="'.$_SESSION['words'][422].' '.$positiveVote->comment.'"></div>';
		}




		//si la proposition n'est pas soutenue par la communauté mais qu'un admin de cette communauté est connecté, lui donner la possibilité de soutenir
		if(empty($positiveVote->endorsed) && Session::exists('communityAdmin') && $community !== false && $community->communityId === Session::get('communityAdmin'))
		//s'il peut voter, mettre les boutons sur les propositions
		if($canVote && $positiveVote->voted < 1){
			$buttons = '<i class="fa fa-thumbs-up font_green clickable" onclick="ajax(\'ajax/buildform.php\',{type:\'votePropositions\',problemId:'.$problemDetails->problemId.'},\'form\')"></i><i class="fa fa-thumbs-down font_red clickable" onclick="ajax(\'ajax/buildform.php\',{type:\'votePropositions\',problemId:'.$problemDetails->problemId.'},\'form\')"></i>';
		}
		$displayPropositions .= '<div class="row"><div class="col-7"><h4><a href="solution-'.$positiveVote->solutionId.'.html"><i class="fas fa-info-circle font_green"></i></a> '.$positiveVote->title.' '.$buttons.'</h4></div><div class="whiteBorder font_white gradient col-3 text-center" style="background-image: linear-gradient(to right, rgb(91, 200, 57) 0%, rgb(91, 200, 57) '.$pertinence.'%, rgb(218, 50, 50) '.$pertinence.'%, rgb(218, 50, 50) 100%);">
									'.$positiveVote->allVotes.' votes sur '.$positiveVote->facetVotes.' utilisateurs impactés</div><div class="col-1 ">'.$barGraph.'</div>'.$ribbon.'</div><span class="col-6 col-offset-1">"'.$positiveVote->description.'"</span></div>';
	}
}
$propsLeft = $problem->countPropsToEvaluate();
$evaluateButton = "";

$user = new user();

$voteButton = "<div id='newVote' onclick='ajax(\"buildform.php\",{type:\"vote\",problemId:".$problemDetails->problemId."},\"form\")' class='clickable'>".$_SESSION['words'][4]." <i class='fa fa-thumbs-up'></i></div>";
$newProp = "<button type='button' onclick='ajax(\"buildform.php\",{type:\"selectSolution\",problemId:".$problemDetails->problemId."},\"form\")' class='clickable font_red white noBorder'><i class='fa fa-plus-circle'></i></button>";
$reportSpam = "<div onclick='ajax(\"buildform.php\",{type:\"spam\",entityId:".$problemDetails->problemId.",entityType:\"problem\"},\"form\")' class='clickable'>".$_SESSION['words'][183]." <i class='fa fa-warning'></i></div> <!-- report -->";

if ($problem->hasVoted()){
	$voteButton = "<div id='newVote'>".$_SESSION['words'][233]."<i class='fa fa-check'></i></div>";//already voted
	if($propsLeft>0){
		$evaluateButton .= "<div class='clickable' onclick='ajax(\"buildform.php\",{type:\"votePropositions\",problemId:".$problemDetails->problemId."},\"form\")'>".$propsLeft." ".$_SESSION['words'][260].helper::plural($propsLeft)." ".$_SESSION['words'][243]."<i class='fa fa-gavel'></i></div>";
	}
}

?>

<div class="container-fluid map-height">
		<div class="row">
				<div itemscope class="col-md-8" itemtype="http://schema.org/ItemPage">
						<span id="infoWindow" style="padding-left: 25vw; color: red; font-weight: bold;"></span>
						<div class="row mbottom text-center" id="infos" data-problemid="<?php echo $problemDetails->problemId; ?>" d-lat="<?php echo $problemDetails->latitude ?>" d-lng="<?php echo $problemDetails->longitude ?>">
								<div class="col-md-2">
										<img itemprop="image" src="img/<?php echo $problemDetails->categoryIcon ?>">
								</div>
								<div class="col-md-8">
										<div class="row">
												<h3  itemprop="name" id="title"><?php echo $problemDetails->title; ?></h3>
				<?php if($editTitle){ ?>
												<a href="#"  title="Il vous reste <?php echo $timeLeft; ?> jours pour éditer le titre" onclick="changeOption('.topic_title');"><i class="fa fa-edit"></i></a>
				<?php } ?>
										</div>
										<div id="topic_descr" itemprop="description" class="row">
											<span id="description" class="col-12 text-left"><?php echo $problemDetails->description ?></span>
								<?php if($problem->isCreator()){ ?>
											<a href="#" class="topic_btn" title="<?php echo $_SESSION['words'][146] ?>" onclick="changeOption('#description');"><i class="fa fa-edit"></i></a>
								<?php } ?>
											<span class="col-12 text-left"><span itemprop="datePublished"><?php echo $_SESSION['words'][9]." ".$problemDetails->createdOn; ?> |
											<a href="profile.php?userId=<?php echo $problemDetails->userId; ?>" itemprop="author"><?php echo $problemDetails->userName; ?></a></span> |
											<?php echo $_SESSION['words'][190].' : ' .$_SESSION['words'][$problemDetails->scopeId]; ?></span> <!-- added on -->
<?php
											echo $handledBy;
								?>
										</div>
										<div id="topic_tags" class="row">
										<span class="col-12 text-left"><strong><?php echo $_SESSION['words'][2] ?> :  </strong> <!-- keywords -->
														<?php
																$i=0;
																$tagsSize = count($problemDetails->tags);
																foreach($problemDetails->tags as $tag){
																		echo "<a class='font-italic' href=\"problems.php?tag=".$tag."\"><u>".$tag."</u></a>" ;
																		if($i<($tagsSize-1))
																				echo ", " ;
																		$i++;
																}
																echo '.' ;
														?>
										</span>
										</div>
								</div>
								<div class="col-md-2">
										 <div class='redRound text-center'><span id="nbVotes"><?php echo $problemDetails->nbVotes ?></span><span id='text'><?php echo "vote".helper::plural($problemDetails->nbVotes) ?></span></div>
								</div>
						</div>
						<div class="row font_white red mbottom">
								<div class="col-md-1">
								</div>
								<div class="col-md-2 whiteBorder text-center sideBorder">
<?php
	echo $notMyProblem;
?>
								</div>
								<div class="col-md-2 whiteBorder text-center sideBorder">
<?php
	echo $voteButton;
?>
								</div>
								<div class="col-md-2 whiteBorder text-center sideBorder">
<?php if($problemDetails->statusId === '1'){ echo $reportSpam; }
?>
								</div>
								<div class="col-md-3 whiteBorder text-center sideBorder">
<?php
	echo $contactProblemUsers;
?>
								</div>
								<div class="col-md-2">
<?php
	echo $evaluateButton;
?>
								</div>
						</div>
						<div id='propositions' class="row">
							<div class="col-md-8">
										<!-- Trigger the modal with a button -->
										<h1><?php echo $_SESSION['words'][148]."(".$problemDetails->nbSolutions.") ".$newProp ?></h1><!-- Propositions -->
							</div>
							<div class='col-md-4 d-flex justify-content-around'>
								<a href="http://www.facebook.com/sharer.php?u=<?php echo "www.solucracy.com/problem-".$problemDetails->problemId.".html&title=".$problemDetails->title; ?>"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
								<a href="http://reddit.com/submit?url=<?php echo "www.solucracy.com/problem-".$problemDetails->problemId.".html&title=".$problemDetails->title; ?>"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
								<a href="https://twitter.com/intent/tweet?url=<?php echo "www.solucracy.com/problem-".$problemDetails->problemId.".html&TEXT=".$problemDetails->title; ?>"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
								<a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=<?php echo "www.solucracy.com/problem-".$problemDetails->problemId.".html"; ?>"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
							</div>
						</div>
<?php
	echo $displayPropositions;
?>
				</div>
				<div class="col-md-4">
					<div id="mapContainer">
						<div id="mapContainerShow" class="problemMap"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo Config::get('GMap_API_Key') ?>" width="605px" height="200px" ></div></div>
							<div id="expandBtn">
								<p id="expandMap"><?php echo $_SESSION['words'][149]; ?></p>
						</div>
					</div>
					<div class="row">
						<h3><?php echo $_SESSION['words'][150]; ?> ?</h3>
						<script type="text/javascript">
							google.charts.load("current", {packages:["corechart"]});
							google.charts.setOnLoadCallback(drawChart);
							function drawChart() {
								var data = google.visualization.arrayToDataTable([
									['<?php echo $_SESSION['words'][151]; ?>', '<?php echo $_SESSION['words'][99]; ?>']
	<?php
	 echo $displayFacets;
	?>
								]);

								var options = {
									colors: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928'],
									chartArea: {  width: "100%", height: "95%" },
								};

								var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
								chart.draw(data, options);
							}
						</script>
						<div id="donutchart" class="full-height"></div>
					</div>
				</div>
		</div>
</div>

<?php
include(Config::get('root_path') . "inc/footer.php");
?>

<script type="text/javascript">

function ProcessVote() {
	ajax('processnew.php',$( "#newVoteForm" ).serialize(),'newVote');
}

$('#newText1').focus(function(){
	$('#new').prop('checked',true);
})

function expandMap(){
	if($('#expandMap').text() === ('<?php echo $_SESSION['words'][149]; ?>')){
		$("#mapContainer").height('70%');
		$("#mapContainerShow").height('100%');
		google.maps.event.trigger(d.map, 'resize');
		$('#expandMap').text('<?php echo $_SESSION['words'][271]; ?>');
	} else {
		$("#mapContainer").height('35%');
		$("#mapContainerShow").height('100%');
		google.maps.event.trigger(d.map, 'resize');
		$('#expandMap').text('<?php echo $_SESSION['words'][149]; ?>');
	}
}

$('#expandMap').click(expandMap);

function processProps() {
	// add logic to check that children of swap newtext are not empty if they're checked
	var itemsChecked = $('form :checked');
	var result = true;
	for (var i = itemsChecked.length - 1; i >= 0; i--) {
		var itemId = itemsChecked[i].id;
		if(itemId.indexOf("new")>= 0){
			var inputId = [itemId.slice(0, 3), 'Text', itemId.slice(3)].join('');
			if($('#'+inputId).val().length < 1){
				result = false;
			}
		}
	}
	if(result){
		ajax('processnew.php',$( "#propositionsForm" ).serialize(),'showAndReload');
	}else{
		showMessage("<?php echo $_SESSION['words'][272]?>",false);
	}
}
</script>

