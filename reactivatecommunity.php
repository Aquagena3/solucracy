<?php
require('core/ini.php');
helper::loadHeader('header.php',array(
	'TITLE'=>$_SESSION['words'][463]
	,'DESCRIPTION'=>$_SESSION['words'][464]));

	if(Input::defined('code')){
		$community = new community();
		//et qu'il correspond bien à une communauté et si ça fait moins de 30 jours
		if($community->checkCode(Input::get('code'))){
			//rediriger vers la page pour saisir les informations de paiement
			echo '<p>Si Yannick avait fait son boulot, vous seriez redirigé(e) vers les infos de paiement...</p>';
		}else{
			//sinon signaler à l'utilisateur qu'il fait n'importe quoi
			echo '<p>'.$_SESSION['words'][481].'</p>';
			return;
		}
	}else{
		echo '<p>Oui ? vous cherchez quelque chose ?</p>';
	}
