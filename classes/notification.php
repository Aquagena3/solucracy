<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class notification {

	public static function create($data){
		//dans l'array $data, mettre : link, title, description, notificationTypeId, userId
		$_db = DB::getInstance();

		$data['createdOn']= date('Y-m-d H:i:s');
		if(!$_db->insert('notification',$data)){
			return false;
		}
		return $_db->lastInsertId();
	}

	public static function insertList($data){
		//changer tout ça en stored procedure
		$_db = DB::getInstance();
		$values = "";
		foreach ($data['userList'] as $user) {
			$values .= '("'.$user->userId.'","'.$data['link'].'","'.$data['title'].'","'.$data['description'].'","'.$data['notificationTypeId'].'","'.$data['statusId'].'")';
			if(!next( $data['userList'])){}else{
				$values.= ",";
			}
		}
		$_db->query("INSERT INTO notification ( `userId`, `link`, `title`, `description`, `notificationTypeId`, `statusId`) VALUES ".$values,array());
		return $_db->debug();
	}

	public static function read($id){
		$_db = DB::getInstance();
		$_db->update('notification', $id, array('statusId'=>8));
	}

	public static function getUnread($id){
		$_db = DB::getInstance();
		$_db->query("select *, DATE_FORMAT(createdOn,'%d %m %Y') as createdOn from notification where userId = ? and statusId = 7 order by createdOn desc", array($id));
		return $_db->results();
	}

	public static function countUnread($id){
		$_db = DB::getInstance();
		$_db->query("select * from notification  where userId = ? and statusId = 7", array($id));
		return $_db->count();
	}

	public static function send($id){
		$_db = DB::getInstance();
		$_db->query("select * from notification  where notificationId = ?", array($id));
		$notification = $_db->first();
		$email = new email();
		if($email->sendNotification($notification)){
			return true;
		}
		return false;
	}

	public static function exists($notificationTypeId,$id){
		$_db = DB::getInstance();
		$_db->query("select * from notif_subscription where userId = ? and notificationTypeId = ?", array($id,$type));
		if($_db->count() > 0){
			return true;
		}
		return false;
	}

	public static function sendPending(){
		$_db = DB::getInstance();
		$query = $_db->query("select u.email, u.userName, n.* from user as u inner join notification as n on n.userId = u.userId inner join notif_subscription as ns on ns.userId = u.userId where n.statusId = 7 and n.notificationTypeId = ns.notificationTypeId order by u.email asc", array());

		if($query->count() > 0){
			$results = $query->results();
			while (count($results) > 0){
				$email = $results[0]->email;
				$new = array_filter($results, function($k)use($email){
					return $k->email === $email;
				});
				$results = array_values(array_filter($results, function($k)use($email){
					return $k->email !== $email;
				}));
				$email = new email();
				$result =$email->sendNotifications($new);
			}
		};
		// voir s'il n'y a pas moyen de filtrer si certains emails n'ont pas marché
		$update = $_db->query("UPDATE notification SET statusId=8 WHERE notificationId in (select id from (select n.notificationId as id from user as u inner join notification as n on n.userId = u.userId inner join notif_subscription as ns on ns.userId = u.userId where n.statusId = 7 and n.notificationTypeId = ns.notificationTypeId) as y)");
		return $update->debug();
	}

	public static function subscribe($id,$notificationTypeId,$entityId){
		$_db = DB::getInstance();
		$_db->query("INSERT IGNORE INTO notif_subscription (userId,notificationTypeId,entityId) VALUES (?,?,?)", array($id,$notificationTypeId,$entityId));
	}

	public static function unsubscribe($id,$notificationTypeId,$entityId){
		$_db = DB::getInstance();
		$_db->query("DELETE FROM `notif_subscription` WHERE userId = ? AND notificationTypeId = ? AND entityId = ?", array($id,$notificationTypeId,$entityId));
	}

	public static function newsletter($email,$statusId){
		$_db = DB::getInstance();
		$_db->query("INSERT INTO newsletter (`email`,statusId,modifiedOn) VALUES (?,?,now()) ON DUPLICATE KEY UPDATE statusId= ?,  modifiedOn = now()", array($email,$statusId,$statusId));
		return $_db->debug();
	}

	public static function loadSubscriptions($id){
		$_db = DB::getInstance();
		$query = $_db->query("SELECT notificationTypeId FROM `notif_subscription` WHERE userId = ?", array($id));
		$results = $query->results();
		$notifications = array();
		foreach ($results as $result) {
			array_push($notifications, $result->notificationTypeId);
		}
		return $notifications;
	}

	public static function loadCommunitySubscriptions($id,$entityId){
		$_db = DB::getInstance();
		$query = $_db->query("SELECT notificationTypeId FROM `notif_subscription` WHERE userId = ? and entityId = ?", array($id,$entityId));
		$results = $query->results();
		$notifications = array();
		foreach ($results as $result) {
			array_push($notifications, $result->notificationTypeId);
		}
		return $notifications;
	}

}

