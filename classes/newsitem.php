<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class newsitem{
	//newsItemTypes qui nécessitent de créer une notification
private $requiresNotification = array(1,3,4,5,11),
//newsItemTypes qui nécessitent de faire une évaluation de badge
	$requiresBadgeEvaluation = array(1,2,3,6,7),
	$_db;

	function __construct(){
		$_db = DB::getInstance();
	}
	//créer un nouvel evénement
	public static function create($data){
		$_db = DB::getInstance();
		$result = "";
		$description = "";
		switch ($data['newsItemTypeId']) {
			//proposition ajoutée ou dépasse 75% de pertinence
			case '1':
			case '2':
				$new = $_db->query("SELECT distinct(s.title) as solutionTitle, pb.title as problemTitle, pb.problemId, p.title as propositionTitle, u.userName, p.userId, (SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM proposition as p
inner join solution as s on s.solutionId = p.solutionId
inner join pertinence as pt on pt.propositionId = p.propositionId
inner join facet as f on f.facetId = pt.facetId
inner join problem as pb on pb.problemId = f.problemId
INNER join user as u on p.userId = u.userId
WHERE p.propositionId = ?", array($data['newsItemTypeId'],$data['propositionId']));
				$result = $new->first();
				$data['problemId'] = $result->problemId;
				$data['userId'] = $result->userId;
				break;
			//problème ajouté
			case '3':
				$new = $_db->query("SELECT pb.title as problemTitle,pb.userId, pb.problemId, (SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM problem as pb
WHERE pb.problemId = ?", array($data['newsItemTypeId'],$data['problemId']));
				$result = $new->first();
				$data['problemId'] = $result->problemId;
				$data['userId'] = $result->userId;
				break;
			//problème transféré
			case '4':
				$new = $_db->query("SELECT distinct(pb.title) as problemTitle, pb.problemId, pb.userId,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description,
(SELECT c.name from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 1) as name,
(SELECT c.communityId from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 0 order by cp.statusUpdate desc limit 1) as communityId,
(SELECT c.name from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 0 order by cp.statusUpdate desc limit 1) as oldCommunityName
FROM problem as pb
WHERE pb.problemId = ?", array($data['newsItemTypeId'],$data['problemId']));
				$result = $new->first();
				$data['problemId'] = $result->problemId;
				$data['userId'] = $result->userId;
				break;
			//communauté vient de créer un compte
			case '5':
				$new = $_db->query("SELECT name,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM community as c
WHERE c.communityId = ?", array($data['newsItemTypeId'],$data['communityId']));
				$result = $new->first();
				break;
			//utilisateur a créé un compte
			case '6':
				$new = $_db->query("SELECT u.userName,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description, u.userId
FROM user as u
WHERE u.userId = ?", array($data['newsItemTypeId'],$data['userId']));
				$result = $new->first();
				break;
			case '7':
			//une solution a été ajoutée
				$new = $_db->query("SELECT u.userName, u.userId, s.title as solutionTitle,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM user as u
inner join solution as s on s.userId = u.userId
WHERE s.solutionId = ?", array($data['newsItemTypeId'],$data['solutionId']));
				$result = $new->first();
				break;
			case '8':
			case '10':
			//une collectivité demande de l'aide / n'a plus besoin d'aide pour un problème
				$new = $_db->query("SELECT c.name, c.communityId, pb.title as problemTitle, pb.problemId,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM problem as pb
inner join communityproblem as cp on cp.problemId = pb.problemId
inner join community as c on c.communityId = cp.communityId
WHERE pb.problemId = ? and cp.statusId = 1", array($data['newsItemTypeId'],$data['problemId']));
				$result = $new->first();
				$data['problemId'] = $result->problemId;
				$data['communityId'] = $result->communityId;
				break;
			case '9':
			//une collectivité apporte son soutien à une proposition
				$new = $_db->query("SELECT distinct(s.title) as solutionTitle,p.userId,c.communityId, pb.title as problemTitle, pb.problemId, c.name,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM proposition as p
inner join solution as s on s.solutionId = p.propositionId
inner join pertinence as pt on pt.propositionId = p.propositionId
inner join facet as f on f.facetId = pt.facetId
inner join problem as pb on pb.problemId = f.problemId
INNER JOIN communityproposition as cp on cp.propositionId = p.propositionId
INNER JOIN community as c on c.communityId = cp.communityId
WHERE p.propositionId = ?", array($data['newsItemTypeId'],$data['propositionId']));
				$result = $new->first();
				$data['problemId'] = $result->problemId;
				$data['communityId'] = $result->communityId;
				$data['userId'] = $result->userId;
				break;
			case '11':
			//un badge a été débloqué par un utilisateur
				$new = $_db->query("SELECT u.userName,
(SELECT ".Session::get('language')." FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = 11) as description,
(SELECT ".Session::get('language')." FROM `language` as l inner join badge as b on b.name = l.id WHERE b.badgeId = ?) as name,
ub.userId
FROM userbadge as ub
inner join user as u on ub.userId = u.userId
where ub.badgeId = ? and ub.scope = ?", array($data['badgeId'],$data['badgeId'],$data['scope']));
				helper::logError('newsitem case 11 :'.json_encode($new->debug()));
				$result = $new->first();
				$data['userId'] = Session::get('user');
				break;
			default:
				echo "Ce type de News n'existe pas";
				break;
		}

		// if(in_array($data['newsItemTypeId'],$this->requiresNotification){
		// 	//créer une notification
		// }
		// if(in_array($data['newsItemTypeId'],$this->requiresBadgeEvaluation){
		// 	//evaluer pour un badge
		// }
		// remplacer tout ce qu'il faut dans la description en fonction du type de news
		if(in_array($data['newsItemTypeId'], array(2,1,3,4,8,9,10))){
			$result->description = str_replace('%problemTitle%',$result->problemTitle,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(2,1,7,9))){
			$result->description = str_replace('%solutionTitle%',$result->solutionTitle,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(6,1,7,11))){
			$result->description = str_replace('%userName%',$result->userName,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(1))){
			$result->description = str_replace('%propositionTitle%',$result->propositionTitle,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(9,10,8,4,5))){
			$result->description = str_replace('%communityName%',$result->name,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(4))){
			$result->description = str_replace('%communityName2%',$result->oldCommunityName,$result->description);
		}
		if(in_array($data['newsItemTypeId'], array(11))){
			$result->description = str_replace('%badgeName%',$result->name,$result->description);
		}
		$data['description'] = $result->description;
		unset($data['scope']);
		$query = $_db->insert('newsitem',$data);
		return $query;
	}
	public static function render($data){
		$card = '<div class="card m-2">
				<div class="card-header">
					%formattedDate%
				</div>
				<div class="card-body">
					<h5 class="card-title">%cardTitle%</h5>
					<p class="card-text">%cardText%</p>
					<a href="%url%" class="btn solucracy_btn">'.$_SESSION['words'][452].'</a>
				</div>
			</div>';
		$card = str_replace('%formattedDate%',$data->formattedDate,$card);
		$card = str_replace('%cardTitle%',$_SESSION['words'][$data->title],$card);
		$card = str_replace('%cardText%',$data->description,$card);
		$card = str_replace('%url%',$data->url,$card);
		return $card;
	}
	public static function getNewsItems($scope,$id = null){
		$_db = DB::getInstance();
		switch ($scope) {
			case 'community':
				$new = $_db->query("SELECT
        *
    FROM
        ((SELECT
            DISTINCT (ni.newsItemId),
            DATE_FORMAT(ni.createdOn,
            '%d %m %Y %T') AS formattedDate,
            ni.description,
          	ni.createdOn,
            nit.titleTranslationId AS title,
            (CASE
                WHEN ni.newsItemTypeId = 1
                OR ni.newsItemTypeId = 2
                OR ni.newsItemTypeId = 3
                OR ni.newsItemTypeId = 4
                OR ni.newsItemTypeId = 8
                OR ni.newsItemTypeId = 9
                OR ni.newsItemTypeId = 10 THEN concat('problem-',
                ni.problemId,
                '.html')
                WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',
                ni.communityId)
                WHEN ni.newsItemTypeId = 6
                OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',
                ni.userId)
                WHEN ni.newsItemTypeId = 7 THEN concat('solution-',
                ni.problemId,
                '.html')
            END) AS url
        FROM
            `newsitem` AS ni
        INNER JOIN
            communityproblem AS cp
                ON cp.problemId = ni.problemId
        INNER JOIN
            newsitemtype AS nit
                ON nit.newsItemTypeId = ni.newsItemTypeId
        WHERE
            ni.communityId = ?
        ORDER BY
            ni.createdOn DESC)
    UNION
    DISTINCT (SELECT
        DISTINCT (ni.newsItemId),
        DATE_FORMAT(ni.createdOn,
        '%d %m %Y %T') AS formattedDate,
        ni.description,
        ni.createdOn,
        nit.titleTranslationId AS title,
        (CASE
            WHEN ni.newsItemTypeId = 1
            OR ni.newsItemTypeId = 2
            OR ni.newsItemTypeId = 3
            OR ni.newsItemTypeId = 4
            OR ni.newsItemTypeId = 8
            OR ni.newsItemTypeId = 9
            OR ni.newsItemTypeId = 10 THEN concat('problem-',
            ni.problemId,
            '.html')
            WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',
            ni.communityId)
            WHEN ni.newsItemTypeId = 6
            OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',
            ni.userId)
            WHEN ni.newsItemTypeId = 7 THEN concat('solution-',
            ni.problemId,
            '.html')
        END) AS url
    FROM
        `newsitem` AS ni
    INNER JOIN
        communityproblem AS cp
            ON cp.problemId = ni.problemId
    INNER JOIN
        newsitemtype AS nit
            ON nit.newsItemTypeId = ni.newsItemTypeId
    WHERE
        (cp.communityId = ?
        AND cp.statusId = 1)
    ORDER BY
        ni.createdOn DESC)
) AS union1
ORDER BY
union1.createdOn DESC limit 10", array($id,$id));
				$result = $new->results();
				break;

			case 'user':
			$communityFilters = '';
			//si c'est l'utilisateur connecté, afficher aussi les news pour les communautés auxquelles il est abonné
			if($id === Session::get('user')){
				$communityFilters = 'or ni.communityId in (select communityId from communitysubscription where userId = ni.userId)';
			}
				$new = $_db->query("SELECT DATE_FORMAT(ni.createdOn,'%d %m %Y %T') as formattedDate, ni.*, nit.titleTranslationId as title,
 (CASE
  WHEN ni.newsItemTypeId = 1 OR ni.newsItemTypeId = 2 OR ni.newsItemTypeId = 3 OR ni.newsItemTypeId = 4 OR ni.newsItemTypeId = 8 OR ni.newsItemTypeId = 9 OR ni.newsItemTypeId = 10 THEN concat('problem-',ni.problemId,'.html')
  WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',ni.communityId)
  WHEN ni.newsItemTypeId = 6 OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',ni.userId)
  WHEN ni.newsItemTypeId = 7 THEN concat('solution-',ni.problemId,'.html')
  END) as url
FROM `newsitem` as ni
INNER JOIN newsitemtype as nit on nit.newsItemTypeId = ni.newsItemTypeId
WHERE ni.userId = ? ".$communityFilters." order by ni.createdOn desc limit 10", array($id));
				$result = $new->results();
				break;

			case 'none':
				# code...
				break;

			default:
				# code...
				break;
		}

		return $result;
	}

	//trouver un événement existant
	private function find($id){}
}


