<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/

class community {

	private $_db,
	$_data,
	$markers;

	function __construct($id=null){
		$this->_db = DB::getInstance();
		if(isset($id) AND is_numeric($id)){
			$this->find('id',$id);
		}

	}

	public function update($fields){
		$this->_db->update('community', $this->_data->communityId, $fields);
	}

	public function updateStatus($value){
		$this->update(array('statusId'=>$value,'statusUpdate'=>date('Y-m-d H:i:s')));
	}

	public function get($arg){
		return $this->_data->$arg;
	}

	public function nameAvailable($name){
		$data = $this->_db->get('community', array('name', '=', $name));
		if($data->count()>0) {
			return false;
		}
		return true;
	}

	public function find($criteria,$value){
		switch ($criteria) {
			case 'name(departmentId)':
				$communityDetails = explode("(",$value);
				$name = $communityDetails[0];
				$departmentId = str_replace(")", "", $communityDetails[1]);
				$new = $this->_db->query("SELECT * ,(select count(distinct(userId)) from communitysubscription as cs where userId = ? and cs.communityId = c.communityId) as follower from community as c where departmentId = ? and name = ?", array(Session::get('user'),$departmentId,$name));
				$this->_data = $new->first();
				return true;
				break;
			case 'id':
				$new = $this->_db->query("SELECT *,(select count(distinct(userId)) from communitysubscription as cs where userId = ? and cs.communityId = c.communityId) as follower from community as c where communityId = ?", array(Session::get('user'),$value));
				$this->_data = $new->first();
				return true;
				break;

			default:
				return false;
				break;
		}
	}

	public function data() {
		return $this->_data;
	}

	public function exists() {
		return (!empty($this->_data)) ? true : false;
	}

	public function subscribe() {
		if(isset($this->_data->communityId)){
			$this->_db->query("INSERT IGNORE INTO communitysubscription (userId,communityId) VALUES (?,?)", array('userId'=>Session::get('user'),'communityId'=>$this->_data->communityId));
			return true;
		}
		return false;
	}
	public function unsubscribe() {
		if(isset($this->_data->communityId)){
			$this->_db->query("DELETE FROM `communitysubscription` WHERE userId = ? AND communityId = ?", array('userId'=>Session::get('user'),'communityId'=>$this->_data->communityId));
			return true;
		}
		return false;
	}

	public function create($data){
		//si c'est une collectivité, elle existe déjà dans la DB
		if($data['communityTypeId']==2){
			//trouver l'Id de la communauté en fonction du nom
			$this->find('name(departmentId)',$data['name']);
			//mettre à jour le statut
			$this->updateStatus(9); //awaiting payment
		} else{
			//sinon il faut la créer
			//vérifier que le nom ne soit pas déjà pris
			if($this->nameAvailable($data['name'])){
				//préparer les données pour la création de la communauté
				$data['statusUpdate']= date('Y-m-d H:i:s');
				$data['statusId'] = 9; //awaiting payment
				//créer la communauté
				if($this->_db->insert('community',$data)){
					$this->find('id',$this->_db->lastInsertId());
				}else{
					return 'problem';
				}
			}else{
				return 'nameNotAvailable';
			}
		}
		//creer l'admin de la communauté
		if($this->_db->insert('communityAdmin',array('userId'=>Session::get('user'),'communityId'=>$this->_data->communityId))){
			return 'created';
		}
	return 'problem';
	}

	public function getAdmins() {
		$new = $this->_db->query("SELECT ca.*,DATE_FORMAT(ca.createdOn,'%d/%m/%Y') as since, u.userName, u.email from communityadmin as ca inner join user as u on ca.userId = u.userId where ca.communityId = ?", array($this->_data->communityId));
				$results = $new->results();
			return $results;
	}
	public function isAdmin($userId){
		$new = $this->_db->query("SELECT count(*) as total from communityadmin where communityId = ? and userId = ?", array($this->_data->communityId,$userId));
				$results = $new->first();
			if($results->total>0){
				return true;
			}else{
				return false;
			}
	}

	public function AddAdmin($email) {
		//check if the person adding actually has the right to
		if($this->isAdmin(Session::get('user'))){
			//check if the person is a Solucracy user
			if(helper::activeUserExists($email)){
				$new = $this->_db->query("INSERT IGNORE INTO communityadmin (userId,communityId) VALUES ((select userId from user where email = ?),?)", array($email,$this->_data->communityId));
				//construire ce qu'il faut envoyer comme notifications
				$new = $this->_db->query("select userId from user where email = ?", array($email));
				$userId = $new->first()->userId;
				$data['userId'] = $userId;
				$data['statusId'] = 7;
				$data['title'] = $_SESSION['words'][404];//You have been chosen as a community admin
				$data['notificationTypeId'] = 8;
				$data['description'] = $this->_data->name." : ".$_SESSION['words'][404];//You have been chosen as a community admin
				$data['link'] = 'communityprofile.php?communityId='.$this->_data->communityId;
				notification::create($data);
			}else{
				return $email." ".$_SESSION['words'][399]; //this person doesn't own a Solucracy account which is necessary to be an admin
			}
		}else{
			return $_SESSION['words'][400];//You don't have the necessary privileges to do this
		}
		return $email." ".$_SESSION['words'][401];//has been added as an admin
	}

	public function deleteAdmin($userId) {
		//check if the person adding actually has the right to
		if($this->isAdmin(Session::get('user'))){
			//check if the person is the last admin and if the community is still active
			if(count($this->getAdmins()) < 2 && $this->_data->statusId != 0){
				echo helper::outcome(402,false);//You are the last admin, please cancel your subscription to delete your admin privileges
				exit();
			} else{
				$new = $this->_db->query("DELETE FROM `communityadmin` WHERE userId = ? and communityId = ?", array($userId,$this->_data->communityId));
				//construire ce qu'il faut envoyer comme notifications
				$data['userId'] = $userId;
				$data['statusId'] = 7;
				$data['title'] = $_SESSION['words'][405];//Your admin rights for a community have been revoked
				$data['notificationTypeId'] = 8;
				$data['description'] = $this->_data->name." : ".$_SESSION['words'][405];//Your admin rights for a community have been revoked
				$data['link'] = 'communityprofile.php?communityId='.$this->_data->communityId;
				notification::create($data);
			}
		}else{
			echo helper::outcome(400,false);//You don't have the necessary privileges to do this
				exit();
		}
		echo helper::outcome(403,true);//That person isn't an admin anymore
				exit();
	}

	public function getUserContactList($type){
		$filterOnCity = "";
		//s'il faut filtrer sur les habitants, ajouter un bout de query, c'est moche et faudra le refaire
		if($type == 2){
			$filterOnCity = ' and concat(c.name,",",c.departmentId) = LEFT(u.city,length(u.city)-3)';
		}
		$new = $this->_db->query("SELECT cs.userId FROM `communitysubscription` as cs inner join user as u on cs.userId = u.userId inner join community as c on c.communityId = cs.communityId inner join notif_subscription as ns on ns.userId = cs.userId WHERE cs.communityId = ? and ns.notificationTypeId = 10".$filterOnCity, array($this->_data->communityId));
		$results = $new->results();
		return $results;
	}

	public function getCommunityProblems(){
		$query = $this->_db->query('SELECT pb.title,c.icon, pb.description, pb.problemId as id,DATE_FORMAT(pb.createdOn,"%d %m %Y") as createdOn,
			(SELECT round(COUNT(DISTINCT v.voteId)) from vote AS v INNER JOIN facet as f ON f.facetId = v.facetId where f.problemId=pb.problemId) AS count, cp.needHelp,(SELECT COUNT(DISTINCT pe.propositionId) from pertinence AS pe INNER JOIN facet as f ON f.facetId = pe.facetId inner join proposition as pro on pro.propositionId = pe.propositionId where f.problemId = pb.problemId and pro.statusId <> 5) AS nbItems
			FROM problem as pb
			INNER JOIN communityproblem as cp on cp.problemId = pb.problemId
			INNER JOIN category as c on pb.categoryId = c.categoryId
			WHERE cp.communityId = ? and pb.statusId <> 5 and cp.statusId = 1 group by pb.problemId',array($this->_data->communityId));

	$results = $query->results();
	// helper::logError($query->debug());
	return $results;

	}
 //activer quand la communauté a payé
	public function activate(){
		//créer le newsItem
		newsitem::create(array('newsItemTypeId'=>5,'communityId'=>$this->_data->communityId));
	}
//désactiver un compte communauté
	public function deactivate(){
		//créer un code
		$code = md5($this->_data->name . Config::get('salt'));
		$admins = $this->getAdmins();
		//envoyer un email à tous les admins avec le lien de réactivation
			$email = new email();
			foreach ($admins as $admin) {
				$email->deactivateCommunity($code,$admin->email);
			}
			$email->sendContactEmail($this->_data->name,Config::get('email'),'annulation de compte');
		$this->update(array('statusId'=>0,'statusUpdate'=>date('Y-m-d H:i:s'),'reactivationCode'=>$code));
		//créer une notification pour les abonnés
		$followers = $this->getUserContactList(1);
		foreach ($followers as $follower) {
			//dans l'array $data, mettre : link, title, description, notificationTypeId, userId
		$notificationData = array('link'=>Config::get('root_path'),'title'=>$_SESSION['words'][459],'description'=>$this->_data->name.' '.$_SESSION['words'][460],'notificationTypeId'=>11,'userId'=>$follower->userId);
		notification::create($notificationData);
		}

		return true;
	}

	public function checkCode($code){
		$query = $this->_db->query('SELECT communityId, statusUpdate from community where reactivationCode = ?',array($code));
		if(count($query->results())>0){
			$result = $query->first();
			$timePassed = strtotime(date('Y-m-d'))-strtotime($result->statusUpdate);
			$timePassed = ceil($timePassed/(60*60*24));
			if($timePassed < 30){
				$this->find('id',$result->communityId);
				return true;
			}
		}
		return false;
	}

}
