<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class Validate {
	private $_passed = false,
	$_errors = array(),
	$_db = null;

	public function __construct() {
		$this->_db = DB::getInstance();
	}

	public function check($source, $items = array()) {
		foreach($items as $item => $rules) {
			foreach($rules as $rule => $rule_value) {

				$value = trim($source[$item]);

				if($rule === 'required' && $rule_value === true && empty($value)) {
					$this->addError("{$item} {$_SESSION['words'][267]}."); //is required
				} else if (!empty($value)) {

					switch($rule) {
						case 'min':
						if(strlen($value) < $rule_value) {
								$this->addError("{$item} {$_SESSION['words'][263]} {$rule_value} {$_SESSION['words'][265]}."); //minimum of X characters
							}
						break;
						case 'max':
						if(strlen($value) > $rule_value) {
							$this->addError("{$item} {$_SESSION['words'][264]} {$rule_value} {$_SESSION['words'][265]}."); //maximum of X characters
						}
						break;
						case 'matches':
						if($value != $source[$rule_value]) {
							$this->addError("{$rule_value} {$_SESSION['words'][266]} {$item}."); //must match
						}
						break;
						case 'captcha':
						if(!helper::checkCaptcha($value)) {
							$this->addError("{$_SESSION['words'][493]}");
						}
						break;
						case 'unique':
						$check = $this->_db->get($rule_value, array($item, '=', $value));
						if($check->count()) {
							$this->addError("{$item} {$_SESSION['words'][262]}"); //is laready taken
						}
						break;
						case 'empty':
						if($value != "") {
							$this->addError("Caught you, Winnie !");
						}
						break;
						case 'header_injection':
						if( stripos($value,'Content-Type:') !== $rule_value ){
							$this->addError($_SESSION['words'][141]);
						}
						break;
						case 'valid_email':
						if( helper::check_email_address($value) !== $rule_value){
							$this->addError('invalid email address');
						}
						break;
						case 'checked':
						if( helper::check_email_address($value) !== $rule_value){
							$this->addError('invalid email address');
						}
						break;
						}
					}
				}
			}

			if(empty($this->_errors)) {
				$this->_passed = true;
			}

			return $this;
		}

		protected function addError($error) {
			$this->_errors[] = $error;
		}

		public function passed() {
			return $this->_passed;
		}

		public function errors() {
			return $this->_errors;
		}
	}
