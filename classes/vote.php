<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class vote {

	private $_db;
	private $_data;



	function __construct($facetId = null){
		$this->_db = DB::getInstance();
		if($facetId AND !$this->find($facetId)){
			$this->create($facetId);
		}
	}

	public function create($facetId){

		$data = array(
			'createdOn'=>date('Y-m-d H:i:s'),
			'facetId'=>$facetId,
			'userId'=>Session::get('user'),
			'latitude'=>Session::get('latitude'),
			'longitude'=>Session::get('longitude'));
		$this->_db->insert('vote',$data);
		$last = $this->_db->lastInsertId();
		$this -> find($last);
		if(isset($last)){
			return true;
		}
		return false;
	}

	public function find($facetId = null) {
		// Check if voteId specified and grab details
		if($facetId) {
			$data = $this->_db->query("SELECT v.*, f.problemId FROM vote as v INNER JOIN facet as f on f.facetId = v.facetId WHERE v.userId = ? AND v.facetId = ?",
				array(Session::get('user'),$facetId));
			if($data->count()) {
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}
	public function alreadyVoted($problemId){
			$data = $this->_db->query("SELECT COUNT(*) FROM vote as v INNER JOIN facet as f on f.facetId = v.facetId WHERE v.userId = ? AND f.problemId = ?",
				array(Session::get('user'),$problemId));
			if($data->count() > 0 ){
				return false;
			}
		return true;
	}
}
