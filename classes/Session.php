<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class Session {
	public static function exists($name) {
		return (isset($_SESSION[$name])) ? true : false;
	}

	public static function get($name) {
		if(isset($_SESSION[$name])){
			return $_SESSION[$name];
		} else {
			return 0;
		}
	}
	public static function getWholeSession(){
		return $_SESSION;
	}

	public static function put($name, $value) {
		return $_SESSION[$name] = $value;
	}

	public static function delete($name) {
		if(self::exists($name)) {
			unset($_SESSION[$name]);
		}
	}

	public static function setLanguage($language){
		return $_SESSION['language'] = $Language;
	}

	public static function flash($name, $string = null) {
		if(self::exists($name)) {
			$session = self::get($name);
			self::delete($name);
			return $session;
		} else if ($string) {
			self::put($name, $string);
		}
	}
}
