<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class badge {
//ajouter un nouveau badge : Trouver une citation, loguer la citation+ les conditions d'évaluation et le nom du badge dans les traductions, créer un icone pour le badge, insérer le nouveau badge dans la table, ajouter les conditions d'évaluation du badge dans la fonction evaluate.
	public static function exists() {
	}

	public static function createUnique($badgeId,$scope,$userId,$categoryId=null){
		$_db = DB::getInstance();
		//if the badge doesn't exist, create it, otherwise update it ( might want to allow for multiple inserts at some point)

		$query = $_db->query("INSERT INTO `userbadge`(`userId`, `badgeId`, `scope`, `displayed`) VALUES (?,?,?,0) on duplicate key UPDATE `displayed`=IF(userId = ?,1,0), userId = ?",
		array($userId,$badgeId,$scope,$userId,$userId));
		//créer le newsItem
		newsitem::create(array('newsItemTypeId'=>11,'badgeId'=>$badgeId,'scope'=>$scope));
		return true;
	}

	public static function createCommon($data){
		$_db = DB::getInstance();
		// if the badge doesn't exist, create it, otherwise update it ( might want to allow for multiple inserts at some point)
		$insert_values = array();
		foreach($data as $d){
			$question_marks[] = '('  . helper::placeholders('?', sizeof($d)) . ')';
			$insert_values = array_merge($insert_values, array_values($d));
		}
		$query = $_db->query("INSERT IGNORE INTO userbadge (`scope`,`userId`, `badgeId` ) VALUES " . implode(',', $question_marks),
		$insert_values);
		foreach($data as $d){
			//créer le newsItem
			newsitem::create(array('newsItemTypeId'=>11,'badgeId'=>$d[2],'scope'=>$d[0]));
		}
		return true;
	}

	public static function evaluate($type,$userId=null) {
		$_db = DB::getInstance();
		if(!$userId){
			$userId = Session::get('user');
		}
		$badges = array();
		switch ($type) {
			case 'newProblem':
				//count nb of problems logged, figure out if he logged the most problem in the city, the world, or the country or if it is in a different country
			$query = $_db->query('SET @user = ?;',array($userId));
			$query = $_db->query("SELECT
				(SELECT
						count(DISTINCT p.problemId)
				FROM
						`problem` AS p
				WHERE
						p.userId = @user
						AND p.statusId <> 5) AS userWorld,
				(SELECT
						count(DISTINCT p.problemId) AS count
				FROM
						`problem` AS p
				WHERE
						p.statusId <> 5
				GROUP BY
						p.userId
				ORDER BY
						count DESC LIMIT 1) AS world,
				(SELECT
						count(DISTINCT p.problemId) AS count
				FROM
						`problem` AS p
				INNER JOIN
						address AS a
								ON p.latitude = a.latitude
								AND p.longitude = a.longitude
				WHERE
						a.country = (
								SELECT
										a.country
								FROM
										address AS a
								INNER JOIN
										user AS u
												ON u.latitude = a.latitude
												AND u.longitude = a.longitude
								WHERE
										u.userId = @user
						)
						AND p.statusId <> 5
				GROUP BY
						p.userId
				ORDER BY
						count DESC LIMIT 1) AS country,
						(SELECT
								count(DISTINCT p.problemId) AS count
						FROM
								`problem` AS p
						INNER JOIN
								user AS u
										ON u.userId = p.userId
						INNER JOIN
								address AS a
										ON p.latitude = a.latitude
										AND p.longitude = a.longitude
						WHERE
								a.locality = (
										SELECT
												a.locality
										FROM
												address AS a
										INNER JOIN
												user AS u
														ON u.latitude = a.latitude
														AND u.longitude = a.longitude
										WHERE
												u.userId = @user
								)
								AND p.statusId <> 5
						GROUP BY
								p.userId
						ORDER BY
								count DESC LIMIT 1) AS city,
								(SELECT
										count(DISTINCT p.problemId) AS count
								FROM
										`problem` AS p
								INNER JOIN
										user AS u
												ON u.userId = p.userId
								INNER JOIN
										address AS a
												ON p.latitude = a.latitude
												AND p.longitude = a.longitude
								WHERE
										a.country <> (
												SELECT
														a.country
												FROM
														address AS a
												INNER JOIN
														user AS u
																ON u.latitude = a.latitude
																AND u.longitude = a.longitude
												WHERE
														u.userId = @user
										)
										AND p.userId = @user
										AND p.statusId <> 5
                                        AND DATE(p.createdOn) <> CURDATE()
								GROUP BY
										p.userId
								ORDER BY
										count DESC) AS tourist,
										(SELECT
												count(DISTINCT p.problemId) AS count
										FROM
												`problem` AS p
										INNER JOIN
												address AS a
														ON p.latitude = a.latitude
														AND p.longitude = a.longitude
										WHERE
												a.country = (
														SELECT
																a.country
														FROM
																address AS a
														INNER JOIN
																user AS u
																		ON u.latitude = a.latitude
																		AND u.longitude = a.longitude
														WHERE
																u.userId = @user
												)
												AND p.userId = @user
												AND p.statusId <> 5
										GROUP BY
												p.userId
										ORDER BY
												count DESC LIMIT 1) AS userCountry,
												(SELECT
														count(DISTINCT p.problemId) AS count
												FROM
														`problem` AS p
												INNER JOIN
														user AS u
																ON u.userId = p.userId
												INNER JOIN
														address AS a
																ON p.latitude = a.latitude
																AND p.longitude = a.longitude
												WHERE
														a.locality = (
																SELECT
																		a.locality
																FROM
																		address AS a
																INNER JOIN
																		user AS u
																				ON u.latitude = a.latitude
																				AND u.longitude = a.longitude
																WHERE
																		u.userId = @user
														)
														AND u.userId = @user
														AND p.statusId <> 5
												GROUP BY
														p.userId
												ORDER BY
														count DESC) AS userCity",array());
			$data = $query->first();
			//create badge depending on criterias : scope + badgeId = unique key for the userbadge table
			if($data->userWorld === $data->world){
				$scope = 'world';
				badge::createUnique(7,$scope,$userId);
			}
			if($data->country >0 && $data->userCountry === $data->country){
				$scope = Session::get('userInfo')->country;
				badge::createUnique(6,$scope,$userId);
			}
			if($data->city >0 && $data->userCity === $data->city){
				$scope = Session::get('userInfo')->city;
				badge::createUnique(5,$scope,$userId);
			}
			if($data->tourist>=1){
				$scope = $userId;
				$badges[] = array($scope,$userId,8);
			}
				//count nb of problems logged, for each category
			$query = $_db->query("SELECT count(distinct problemId) as count, categoryId from problem where userId = ? and statusId <> 5 group by categoryId",array($userId));
			$data = $query->results();
			//créer tous les badges avec des pkey qui contiennent la catégorie : 1ere solution catégorie 1 = 11 , catégorie 2 = 12
			foreach ($data as $item) {
				if($item->count == 1){
					$scope = $userId;
					$badgeId = "279".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 10){
					$scope = $userId;
					$badgeId = "280".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 100){
					$scope = $userId;
					$badgeId = "309".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
			}
			break;
			case 'newSolution':
				//count nb of solutions logged, for each category
			$query = $_db->query("SELECT count(distinct solutionId) as count, categoryId from solution where userId = ? and statusId <> 5 group by categoryId",array($userId));
			$data = $query->results();
			//créer tous les badges avec des pkey qui contiennent la catégorie : name+categorie = badgeId
			foreach ($data as $item) {
				if($item->count == 1){
					$scope = $userId;
					$badgeId = "287".$item->categoryId;
					//badgeId = name + categoryId
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 10){
					$scope = $userId;
					$badgeId = "288".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 100){
					$scope = $userId;
					$badgeId = "289".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
			}
			break;
			case 'newPertinenceVote':
			// compter le nombre de propositions dont la pertinence est supérieur à 75% et qui sont posées par la personne qui a posé la proposition sur laquelle on vote pour chaque catégorie
			$query = $_db->query('SET @user = ?;',array($userId));
			$query = $_db->query("SELECT count(categoryId) as count, categoryId from (
				SELECT
				count(DISTINCT propositionId) AS count,
				categoryId
		FROM
				(SELECT
						count(DISTINCT pertinencevoteId) AS positive,
						p.propositionId,
						NULL AS total,
						prob.categoryId
				FROM
						pertinenceVote AS pv
				INNER JOIN
						pertinence AS p
								ON p.pertinenceId = pv.pertinenceId
				INNER JOIN
						proposition AS pro
								ON p.propositionId = pro.propositionId
								AND pro.userId = @user
				LEFT JOIN
						facet AS f
								ON f.facetId = p.facetId
				LEFT JOIN
						problem AS prob
								ON prob.problemId = f.problemId
				WHERE
						p.positive = 1
				GROUP BY
						p.propositionId
				UNION
				SELECT
						NULL AS positive,
						p.propositionId,
						count(DISTINCT pertinencevoteId) AS total,
						prob.categoryId
				FROM
						pertinenceVote AS pv
				INNER JOIN
						pertinence AS p
								ON p.pertinenceId = pv.pertinenceId
				LEFT JOIN
						facet AS f
								ON f.facetId = p.facetId
				LEFT JOIN
						problem AS prob
								ON prob.problemId = f.problemId
				INNER JOIN
						proposition AS pro
								ON p.propositionId = pro.propositionId
								AND pro.userId = @user
				GROUP BY
						p.propositionId
		) AS counts
GROUP BY
		propositionId,
		categoryId
HAVING
		100 * SUM(positive) / SUM(total) < 101
		AND 100 * SUM(positive) / SUM(total) > 75
ORDER BY
		NULL) as final group by final.categoryId");
			$data = $query->results();
			foreach ($data as $item) {
				if($item->count == 1){
					$scope = $userId;
					$badgeId = "290".$item->categoryId;
					//badgeId = name + categoryId
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 10){
					$scope = $userId;
					$badgeId = "291".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 100){
					$scope = $userId;
					$badgeId = "292".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
			}
				//count nb of propositions logged with a pertinence higher than 75%, figure out if the user logged the most propositions in the city, the world, or the country
			$query = $_db->query('SET @user = ?;',array($userId));
			$query = $_db->query("SELECT
				(SELECT
						Count(DISTINCT prop.propositionid) AS nbProps
				FROM
						proposition AS prop
				INNER JOIN
						pertinence AS p
								ON p.propositionid = prop.propositionid
				INNER JOIN
						facet AS f
								ON f.facetid = p.facetid
				INNER JOIN
						problem AS prob
								ON prob.problemid = f.problemid
				WHERE
						EXISTS (
								SELECT
										1
								FROM
										proposition AS proposition1
								WHERE
										prop.propositionid = proposition1.propositionid
								HAVING
										(
												SELECT
														Count(DISTINCT pv.pertinencevoteid) AS COUNT
												FROM
														pertinencevote AS pv
												INNER JOIN
														pertinence AS p
																ON p.pertinenceid = pv.pertinenceid
												WHERE
														p.propositionid = prop.propositionid
														AND p.positive = 1
										) / (
												SELECT
														Count(DISTINCT pv.pertinencevoteid) AS COUNT
												FROM
														pertinencevote AS pv
												INNER JOIN
														pertinence AS p
																ON p.pertinenceid = pv.pertinenceid
												WHERE
														p.propositionid = prop.propositionid
										) > 0.75
								)
								AND prop.userid = @user
								AND prop.statusId <> 5
						) AS userWorld, (
								SELECT
										Count(DISTINCT prop.propositionid) AS nbProps
								FROM
										proposition AS prop
								INNER JOIN
										pertinence AS p
												ON p.propositionid = prop.propositionid
								INNER JOIN
										facet AS f
												ON f.facetid = p.facetid
								INNER JOIN
										problem AS prob
												ON prob.problemid = f.problemid
								WHERE
										EXISTS (
												SELECT
														1
												FROM
														proposition AS proposition2
												WHERE
														prop.propositionid = proposition2.propositionid
												HAVING
														(
																SELECT
																		Count(DISTINCT pv.pertinencevoteid) AS COUNT
																FROM
																		pertinencevote AS pv
																INNER JOIN
																		pertinence AS p
																				ON p.pertinenceid = pv.pertinenceid
																WHERE
																		p.propositionid = prop.propositionid
																		AND p.positive = 1
														) / (
																SELECT
																		Count(DISTINCT pv.pertinencevoteid) AS COUNT
																FROM
																		pertinencevote AS pv
																INNER JOIN
																		pertinence AS p
																				ON p.pertinenceid = pv.pertinenceid
																WHERE
																		p.propositionid = prop.propositionid
														) < 1
												)
												AND prop.userid = @user
												AND prop.statusId <> 5
										) AS nonPertinent, (
												SELECT
														Count(DISTINCT prop.propositionid) AS nbProps
												FROM
														proposition AS prop
												INNER JOIN
														pertinence AS p
																ON p.propositionid = prop.propositionid
												INNER JOIN
														facet AS f
																ON f.facetid = p.facetid
												INNER JOIN
														USER AS u
																ON u.userId = prop.userId
												INNER JOIN
														problem AS prob
																ON prob.problemid = f.problemid
												INNER JOIN
														address AS a
																ON a.latitude = prob.latitude
																AND a.longitude = prob.longitude
												WHERE
														EXISTS (
																SELECT
																		1
																FROM
																		proposition AS proposition3
																WHERE
																		prop.propositionid = proposition3.propositionid
																HAVING
																		(
																				SELECT
																						Count(DISTINCT pv.pertinencevoteid) AS COUNT
																				FROM
																						pertinencevote AS pv
																				INNER JOIN
																						pertinence AS p
																								ON p.pertinenceid = pv.pertinenceid
																				WHERE
																						p.propositionid = prop.propositionid
																						AND p.positive = 1
																		) / (
																				SELECT
																						Count(DISTINCT pv.pertinencevoteid) AS COUNT
																				FROM
																						pertinencevote AS pv
																				INNER JOIN
																						pertinence AS p
																								ON p.pertinenceid = pv.pertinenceid
																				WHERE
																						p.propositionid = prop.propositionid
																		) > 0.75
																)
																AND prop.userid = @user
																AND a.country = (
																		SELECT
																				a.country
																		FROM
																				address AS a
																		INNER JOIN
																				user AS u
																						ON u.latitude = a.latitude
																						AND u.longitude = a.longitude
																		WHERE
																				u.userId = @user
																)
																AND prop.statusId <> 5
																AND prob.statusId <> 5
														) AS userCountry, (
																SELECT
																		Count(DISTINCT prop.propositionid) AS nbProps
																FROM
																		proposition AS prop
																INNER JOIN
																		pertinence AS p
																				ON p.propositionid = prop.propositionid
																INNER JOIN
																		facet AS f
																				ON f.facetid = p.facetid
																INNER JOIN
																		USER AS u
																				ON u.userId = prop.userId
																INNER JOIN
																		problem AS prob
																				ON prob.problemid = f.problemid
																INNER JOIN
																		address AS a
																				ON a.latitude = prob.latitude
																				AND a.longitude = prob.longitude
																WHERE
																		EXISTS (
																				SELECT
																						1
																				FROM
																						proposition AS proposition4
																				WHERE
																						prop.propositionid = proposition4.propositionid
																				HAVING
																						(
																								SELECT
																										Count(DISTINCT pv.pertinencevoteid) AS COUNT
																								FROM
																										pertinencevote AS pv
																								INNER JOIN
																										pertinence AS p
																												ON p.pertinenceid = pv.pertinenceid
																								WHERE
																										p.propositionid = prop.propositionid
																										AND p.positive = 1
																						) / (
																								SELECT
																										Count(DISTINCT pv.pertinencevoteid) AS COUNT
																								FROM
																										pertinencevote AS pv
																								INNER JOIN
																										pertinence AS p
																												ON p.pertinenceid = pv.pertinenceid
																								WHERE
																										p.propositionid = prop.propositionid
																						) > 0.75
																				)
																				AND a.country = (
																						SELECT
																								a.country
																						FROM
																								address AS a
																						INNER JOIN
																								user AS u
																										ON u.latitude = a.latitude
																										AND u.longitude = a.longitude
																						WHERE
																								u.userId = @user
																				)
																				AND prob.statusId <> 5
																		GROUP BY
																				prop.userId
																		ORDER BY
																				nbProps LIMIT 1) AS country,
																				(SELECT
																						Count(DISTINCT prop.propositionid) AS nbProps
																				FROM
																						proposition AS prop
																				INNER JOIN
																						pertinence AS p
																								ON p.propositionid = prop.propositionid
																				INNER JOIN
																						facet AS f
																								ON f.facetid = p.facetid
																				INNER JOIN
																						USER AS u
																								ON u.userId = prop.userId
																				INNER JOIN
																						problem AS prob
																								ON prob.problemid = f.problemid
																				INNER JOIN
																						address AS a
																								ON a.latitude = prob.latitude
																								AND a.longitude = prob.longitude
																				WHERE
																						EXISTS (
																								SELECT
																										1
																								FROM
																										proposition AS proposition5
																								WHERE
																										prop.propositionid = proposition5.propositionid
																								HAVING
																										(
																												SELECT
																														Count(DISTINCT pv.pertinencevoteid) AS COUNT
																												FROM
																														pertinencevote AS pv
																												INNER JOIN
																														pertinence AS p
																																ON p.pertinenceid = pv.pertinenceid
																												WHERE
																														p.propositionid = prop.propositionid
																														AND p.positive = 1
																										) / (
																												SELECT
																														Count(DISTINCT pv.pertinencevoteid) AS COUNT
																												FROM
																														pertinencevote AS pv
																												INNER JOIN
																														pertinence AS p
																																ON p.pertinenceid = pv.pertinenceid
																												WHERE
																														p.propositionid = prop.propositionid
																										) > 0.75
																								)
																								AND prop.userid = @user
																								AND a.locality = (
																										SELECT
																												a.locality
																										FROM
																												address AS a
																										INNER JOIN
																												user AS u
																														ON u.latitude = a.latitude
																														AND u.longitude = a.longitude
																										WHERE
																												u.userId = @user
																								)
																								AND prop.statusId <> 5
																								AND prob.statusId <> 5
																						) AS userCity, (
																								SELECT
																										Count(DISTINCT prop.propositionid) AS nbProps
																								FROM
																										proposition AS prop
																								INNER JOIN
																										pertinence AS p
																												ON p.propositionid = prop.propositionid
																								INNER JOIN
																										facet AS f
																												ON f.facetid = p.facetid
																								INNER JOIN
																										USER AS u
																												ON u.userId = prop.userId
																								INNER JOIN
																										problem AS prob
																												ON prob.problemid = f.problemid
																								INNER JOIN
																										address AS a
																												ON a.latitude = prob.latitude
																												AND a.longitude = prob.longitude
																								WHERE
																										EXISTS (
																												SELECT
																														1
																												FROM
																														proposition AS proposition6
																												WHERE
																														prop.propositionid = proposition6.propositionid
																												HAVING
																														(
																																SELECT
																																		Count(DISTINCT pv.pertinencevoteid) AS COUNT
																																FROM
																																		pertinencevote AS pv
																																INNER JOIN
																																		pertinence AS p
																																				ON p.pertinenceid = pv.pertinenceid
																																WHERE
																																		p.propositionid = prop.propositionid
																																		AND p.positive = 1
																														) / (
																																SELECT
																																		Count(DISTINCT pv.pertinencevoteid) AS COUNT
																																FROM
																																		pertinencevote AS pv
																																INNER JOIN
																																		pertinence AS p
																																				ON p.pertinenceid = pv.pertinenceid
																																WHERE
																																		p.propositionid = prop.propositionid
																														) > 0.75
																												)
																												AND a.locality = (
																														SELECT
																																a.locality
																														FROM
																																address AS a
																														INNER JOIN
																																user AS u
																																		ON u.latitude = a.latitude
																																		AND u.longitude = a.longitude
																														WHERE
																																u.userId = @user
																												)
																												AND prop.statusId <> 5
																												AND prob.statusId <> 5
																										GROUP BY
																												prop.userId
																										ORDER BY
																												nbProps LIMIT 1) AS city,
																												(SELECT
																														Count(DISTINCT prop.propositionid) AS nbProps
																												FROM
																														proposition AS prop
																												INNER JOIN
																														pertinence AS p
																																ON p.propositionid = prop.propositionid
																												INNER JOIN
																														facet AS f
																																ON f.facetid = p.facetid
																												INNER JOIN
																														USER AS u
																																ON u.userId = prop.userId
																												INNER JOIN
																														problem AS prob
																																ON prob.problemid = f.problemid
																												WHERE
																														prop.statusId <> 5
																														AND prob.statusId <> 5
																														AND EXISTS (
																																SELECT
																																		1
																																FROM
																																		proposition AS proposition7
																																WHERE
																																		prop.propositionid = proposition7.propositionid
																																HAVING
																																		(
																																				SELECT
																																						Count(DISTINCT pv.pertinencevoteid) AS COUNT
																																				FROM
																																						pertinencevote AS pv
																																				INNER JOIN
																																						pertinence AS p
																																								ON p.pertinenceid = pv.pertinenceid
																																				WHERE
																																						p.propositionid = prop.propositionid
																																						AND p.positive = 1
																																		) / (
																																				SELECT
																																						Count(DISTINCT pv.pertinencevoteid) AS COUNT
																																				FROM
																																						pertinencevote AS pv
																																				INNER JOIN
																																						pertinence AS p
																																								ON p.pertinenceid = pv.pertinenceid
																																				WHERE
																																						p.propositionid = prop.propositionid
																																		) > 0.75
																																)
																														) AS world",array());
			$data = $query->first();
			//create badge depending on criterias : scope + badgeId = unique key for the userbadge table
			if($data->userWorld === $data->world){
				$scope = 'world';
				badge::createUnique(15,$scope,$userId);
			}
			if($data->userCountry === $data->country){
				$scope = Session::get('userInfo')->country;
				badge::createUnique(14,$scope,$userId);
			}
			if($data->city >0 && $data->userCity === $data->city){
				$scope = Session::get('userInfo')->city;
				badge::createUnique(13,$scope,$userId);
			}
			if($data->nonPertinent == 10){
				$scope = $userId;
				badge::createUnique(18,$scope,$userId);
			}
			break;
			case 'newProposition':
				//count nb of solutions logged, for each category
			$query = $_db->query('SET @user = ?',array($userId));
			$query = $_db->query("SELECT
        count(DISTINCT prop.propositionId) AS count,
        prob.categoryId
    FROM
        proposition AS prop
    INNER JOIN
        pertinence AS p
            ON p.propositionid = prop.propositionid
    INNER JOIN
        facet AS f
            ON f.facetid = p.facetid
    INNER JOIN
        problem AS prob
            ON prob.problemid = f.problemid
    INNER JOIN
        solution AS s
            ON prop.solutionId = s.solutionId
    WHERE
        prop.userId = @user
        AND prob.userId <> @user
        AND s.userId <> @user
        AND prop.statusId <> 5
    GROUP BY
        prob.categoryId
    ORDER BY
        NULL",array());
			$data = $query->results();
			//créer tous les badges avec des pkey qui contiennent la catégorie :  name+categorie = badgeId
			foreach ($data as $item) {
				if($item->count == 1){
					$scope = $userId;
					$badgeId = "297".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);

				}
				if($item->count == 10){
					$scope = $userId;
					$badgeId = "298".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}
				if($item->count == 100){
					$scope = $userId;
					$badgeId = "299".$item->categoryId;
					$badges[] = array($scope,$userId,$badgeId);
				}

			}
			break;
			case 'newProblemVote':
				//count nb of solutions logged, for each category
			$query = $_db->query("SELECT count(voteId) as count FROM `vote` WHERE userId = ?",array($userId));
			$data = $query->first();
			if($data->count == 1){
				badge::createUnique(26,$userId,$userId);
			}
			break;
			case 'newSpamProcessed':
				//count nb of approved spams logged by that person
			$query = $_db->query("SELECT count(*) as count FROM `spam` WHERE flaggedBy = ? and approved = 1",array($userId));
			$data = $query->first();
			if($data->count == 1){
				badge::createUnique(19,$userId,$userId);
			}
			if($data->count == 10){
				badge::createUnique(20,$userId,$userId);
			}
			$query = $_db->query("SELECT (SELECT count(*) FROM `spam` WHERE flaggedBy = ? and approved = 1)/(SELECT count(*) FROM `spam` WHERE flaggedBy = ?) as ratio, (SELECT count(*) FROM `spam` WHERE flaggedBy = ? and approved = 1) as total",array($userId,$userId,$userId));
			$data = $query->first();
			if($data->ratio > 0.8 && $data->total >= 10){
				badge::createUnique(21,$userId,$userId);
				$user = new user($userId);
				$user->addUserRole($userId,'moderator');
			}
			$query = $_db->query("SELECT count(*) as total FROM `spam` WHERE processedBy = ?",array(Session::get('user')));
			$data = $query->first();
			if($data->total == 50){
				badge::createUnique(22,Session::get('user'),Session::get('user'));
				$loggedIn = new user();
				$loggedIn->addUserRole(Session::get('user'),'editor');
			}
			break;
			case 'propositionSupport':
				//compter le nombre de propositions créées par cet utilisateur qui sont soutenues par une communauté ( et oui, je me sers de la variable userId pour stocker propositionId, c'est nul et ça peut créer des confusions)
			$query = $_db->query("SELECT count(*) as count, p.userId FROM `proposition` as p inner join communityproposition as cp on cp.propositionId = p.propositionId WHERE p.userId = (select userId from proposition where propositionId = ?)",array($userId));
			$data = $query->first();
			//s'il y en 10, créer le badge.
			if($data->count == 10){
				$badges[] = array($data->userId,$data->userId,31);
			}
			break;
			default:
				# code...
			break;
		}
		if(count($badges)>0){
			badge::createCommon($badges);
		}
	}

	public static function getNewBadges() {
		$_db = DB::getInstance();
		//get badges that haven't been seen yet
		$query = $_db->query("SELECT b.image, l.".Session::get('language')." FROM `userbadge` as ub inner join badge as b on ub.badgeId = b.badgeId inner join language as l on b.name = l.id WHERE userId = ? and displayed = 0",
		array(Session::get('user')));
		$data = $query->results();
		return $data;
	}
	public static function badgesDisplayed() {
		$_db = DB::getInstance();
		$update = $_db->query("UPDATE userbadge SET displayed=1 WHERE userId = ?",
		array(Session::get('user')));
		return true;
	}

	public static function getBadges($userId){
		$_db = DB::getInstance();
		$query = $_db->query("SELECT b.image, l.".Session::get('language')." as name, l2.".Session::get('language')." as quote, b.author, b.categoryId FROM `userbadge` as ub inner join badge as b on ub.badgeId = b.badgeId left join language as l on b.name = l.id left join language as l2 on b.quote = l2.id WHERE ub.userId = ? order by b.categoryId asc, b.badgeId",
		array($userId));
		$data = $query->results();
		return $data;
	}

	public static function topFour($countryId){
		$_db = DB::getInstance();
		$query = $_db->query("SELECT b.image, l.".Session::get('language')." as name, u.userName, u.userId,
			(CASE
	WHEN b.badgeId in (7,15) THEN 'world'
	WHEN b.badgeId in (6,14) THEN 'country'
	END) as scope
FROM `userbadge` as ub
inner join badge as b on ub.badgeId = b.badgeId
inner join user as u on u.userId = ub.userId
left join language as l on b.name = l.id
WHERE b.badgeId in (7,15) OR (b.badgeId in (6,14) AND ub.scope = ?) order by ub.scope desc",
		array($countryId));
		$data = $query->results();
		return $data;
	}
}
