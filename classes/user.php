<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class user {

	private $_db,
	$_data = null,
	$_sessionName = null,
	$_isLoggedIn = false;


	function __construct($user = null){
		$this->_db = DB::getInstance();
		$this->_sessionName = Config::get('session/session_name');


			// Check if a session exists and set user if so.
		if(Session::exists($this->_sessionName) && !$user) {
			$user = Session::get($this->_sessionName);

			if($this->find($user)) {
				$this->_isLoggedIn = true;
			} else {
				$this->logout();
			}
		} else {
			$this->find($user);
		}

	}

	public function find($user = null) {
		// Check if user_id specified and grab details
		if($user) {
			$field = (is_numeric($user)) ? 'userId' : 'email';
			$data = $this->_db->query("SELECT u.userId,u.password, u.userName, u.email, u.createdOn, u.latitude, u.longitude, u.statusId,u.language,u.passwordRequestId,u.requestTime,u.communityId,a.locality as city, a.country FROM `user` as u
				left join address as a on a.longitude = u.longitude AND a.latitude = u.latitude
				WHERE ".$field." = ?",array($user));
			if($data->count()) {
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}

	public function emailAvailable($email){
		$data = $this->_db->get('user', array('email', '=', $email));
		if($data->count()>0) {
			return false;
		}
		return true;
	}

	public function nameAvailable($name){
		$data = $this->_db->get('user', array('userName', '=', $name));
		if($data->count()>0) {
			return false;
		}
		return true;
	}

	public function phoneAvailable($phone){
		$data = $this->_db->query("SELECT phone FROM `user` WHERE phone = ? and userId <> ?",
		array($phone,$this->data()->userId));
		if($data->count()>0) {
			return false;
		}
		return true;
	}

	public function getCommunityLinks(){
		$query = $this->_db->query("SELECT l.userId, u.userName FROM `communityproblem` as l left join community as p on l.communityId = p.communityId join user as u on p.userId = u.userId WHERE l.userId = ?",
				array($this->data()->userId));
			return $query->results();
	}

	public function getCommunities(){
		$query = $this->_db->query("SELECT c.communityId as id, c.name as name FROM `communitysubscription` as cs left join community as c on cs.communityId = c.communityId WHERE cs.userId = ?",
				array($this->data()->userId));
			return $query->results();
	}
//trouver toutes les communautés pour lesquelles cet utilisateur est administrateur
	public function getUserCommunityAdmins(){
		$query = $this->_db->query("SELECT c.name,c.communityTypeId,ca.communityId as id FROM `communityadmin` as ca
inner join community as c on ca.communityId = c.communityId
WHERE ca.userId = ?",
				array($this->data()->userId));
			return $query->results();
	}

	public function update($fields = array(), $id = null) {
		if(!$id && $this->isLoggedIn()) {
			$id = $this->data()->userId;
		}
		if(!$this->_db->update('user', $id, $fields)) {
			return $this->_db->debug();
		}
		return $this->_db->debug();
	}
	public function count($type){
		switch ($type) {
			case 'vote':
			$query = $this->_db->query("SELECT COUNT(*) as total FROM `vote` as v LEFT JOIN problem AS p ON v.problemId = p.problemId WHERE v.userId = ? AND p.statusId in (1,2,3,4)",
				array($this->_data->userId));
			return $query->first()->total;
			break;
			case 'problem':
			$query = $this->_db->query("SELECT COUNT(*) as total FROM `problem` WHERE userId = ? and statusId <> 0",
				array($this->_data->userId));
			return $query->first()->total;
			break;
			case 'comment':
			$query = $this->_db->query("SELECT COUNT(*) as total FROM `comment` WHERE userId = ? and spam = 0",
				array($this->_data->userId));
			return $query->first()->total;
			break;
			case 'userProblemVotes':
			$query = $this->_db->query("SELECT COUNT(*) as total FROM `vote` as v inner JOIN problem AS p ON v.problemId = p.problemId WHERE p.userId = ? AND p.statusId in (1,2,3,4)",
				array($this->_data->userId));
			return $query->first()->total;
			break;
			case 'userCommentVotes':
			$query = $this->_db->query("SELECT SUM( cv.value ) AS total FROM  `commentvote` AS cv INNER JOIN COMMENT AS c ON cv.commentId = c.commentId WHERE c.userId = ? AND c.spam =0",
				array($this->_data->userId));
			return $query->first()->total;
			break;

			default:
			return 'no type selected';
			break;
		}
		$query = $this->_db->query("SELECT COUNT(*) as total FROM `vote` WHERE userId = ?",
			array($this->_data->userId));
		return $query->first()->total;
	}


	public function updateStatus($value,$id = null){
		if(is_null($id)){
			$id = $this->data()->userId;
		}
		if($this->update(array('statusId'=>$value),$id)){
			$this->_data->statusId = $value;
			return true;
		}
		return false;
	}

	public function refreshSession(){
		$this->find(Session::get($this->_sessionName));
		Session::put('userInfo', $this->data());
		Session::get('userInfo')->password = "";
	}

	public function login($email = null, $password = null) {
		//try to find user by email
		$user = $this->find($email);
		//if it exists
		if($user) {
			//session name = userid and fill userinfo with data
			if($this->checkPassword($password)) {
				Session::put($this->_sessionName, $this->data()->userId);
				Session::put('userInfo', $this->data());
				Session::get('userInfo')->password = "";
				$this->_isLoggedIn = true;
				return true;
			}
		}else{
			return 'noEmail';
		}
		$this->logout();
		return false;
	}

	public function findCommunityId($userCity){
		$search = explode(',',$userCity);
		$search[1] = substr($search[1],0,2);
		$search[0] = str_replace(" ", "", $search[0]);
		$query = $this->_db->query("SELECT communityId FROM `community` as c WHERE REPLACE(c.name, ' ', '') = ? AND c.departmentId = ?",
			array($search[0],$search[1]));
		if($query->count()>0) {
			return $query->first()->communityId;
		}
		//si on ne trouve pas la ville dans la liste existante, alors on logue un 0 dans communityId
		return 0;
	}

	private function checkIfAdmin($communityId){
		$query = $this->_db->query("SELECT c.name,ca.communityId FROM `communityadmin` as ca
inner join community as c on ca.communityId = c.communityId
WHERE ca.userId = ? AND ca.communityId = ?",
			array($this->_data->userId,$communityId));
		if($query->count()>0){
			Session::put('communityName', $query->first()->name);
			return TRUE;
		}
		return FALSE;
	}
//add info to the session to know that the person is logged as an admin for a community and which
	public function loginAsAdmin($communityId){
		if($this->checkIfAdmin($communityId)){
			Session::put('communityAdmin', $communityId);
			return true;
		}else{
			Session::delete('communityName');
			Session::delete('communityAdmin');
			return false;
		}
	}

	public function exists() {
		return (!empty($this->_data)) ? true : false;
	}

	public function checkPassword($password = null){
		if($this->data()->password === md5($password . Config::get('salt'))) {
			return true;
		}
		return false;
	}

	public function checkPhoneCode($code){
		$sent = $this->get("activationCode");
		if($sent === $code) {
			return true;
		}
		return false;
	}
	public function getPasswordRequestId($email){
		$query = $this->_db->query("SELECT passwordRequestId FROM user WHERE email = ?",array($email));
		return $query->first()->passwordRequestId;
	}

	public function newPasswordRequest($email){
		if(!$this->emailAvailable($email)){
			$passwordRequestId = md5(helper::test_input(helper::generateRandomString(9)) . Config::get('salt'));
			$query = $this->_db->query("UPDATE user
								SET passwordRequestId= CASE
									WHEN DATE(requestTime)<>CURDATE() OR requestTime IS NULL THEN ?
									ELSE passwordRequestId
								END,
								requestTime = now()
								WHERE email = ?",
				array($passwordRequestId,$email));

				$notification = new email();
				//Send validation email
				$newCode = $this->getPasswordRequestId($email);
				if ($notification->passwordRequest($email,$newCode)) {
					return true;
				}
			return false;
		}
		return false;
	}

	public function get($arg){
		return $this->_data->$arg;
	}

	public function getNbSpams(){
		$query = $this->_db->query('SET @userId = ?',array($this->_data->userId));
		$query = $this->_db->query("SELECT ROUND(((SELECT count(*)
FROM `spam` as sp
inner join solution as s on s.solutionId = sp.entityId
WHERE s.userId = @userId and sp.entity = 'solution' and sp.approved = 1)+
(SELECT count(*)
FROM `spam` as sp
inner join problem as p on p.problemId = sp.entityId
WHERE p.userId =  @userId and sp.entity = 'problem')+
(SELECT count(*)
FROM `spam` as sp
inner join proposition as pr on pr.propositionId = sp.entityId
WHERE pr.userId =  @userId and sp.entity = 'proposition'))*100 /
(SELECT (select count(*)
FROM problem where userId =  @userId)+(select count(*)
FROM solution where userId =  @userId)+(select count(*)
FROM proposition where userId =  @userId)),0) as spamScore",
			array());
		$result = $query->first()->spamScore;
		return $result;
	}

	public function create($data = array()){
			// if checks are ok, create all variables
		$name = helper::test_input($data['userName']);
		$email = helper::test_input($data['email']);
		$password = md5(helper::test_input($data["password"]) . Config::get('salt'));

				// insert new user in DB
		$last = $this->_db->insert('user',array(
			'userName'=>$name,
			'password'=>$password,
			'language'=>Session::get('language'),
			'latitude'=>'0.00000000',
			'longitude'=>'0.00000000',
			'email'=>$email,
			'statusId'=>0
			));
		$last = $this->_db->lastInsertId();
		$this->addUserRole($last,'utilisateur');
		if(isset($last)){
			$this -> find($last);
			return true;
		} else {
			return false;
		}

	}
//plus besoin de cette fonction, attendre un peu avant d'effacer 4/10/2018
	// public function addLocation($data){
	// 	$coordinates = explode(',', trim($data, '()'));
	// 	$this->update(array('latitude'=>$coordinates[0],'longitude'=>$coordinates[1]));
	// }

	public function addUserRole($id,$role){
		$query = $this->_db->query("INSERT INTO userrole SET userId = ?,roleId = ?",
			array($id,$role));
	}

	public function checkRole($role){
		$query = $this->_db->query("SELECT * FROM userrole
			WHERE userId = ? AND roleId = ?",
			array($this->_data->userId,$role));
		if($query->count()>0){
			return TRUE;
		}
		return FALSE;
	}
	public function isLoggedIn() {
		return $this->_isLoggedIn;
	}

	public function data() {
		return $this->_data;
	}

	public function delete(){
		$query = $this->_db->query("DELETE FROM notif_subscription WHERE userId = ?",array($this->_data->userId));
		$delete['userName'] = $_SESSION['words'][278];
		$delete['password'] = md5($this->_data->email);
		$delete['email'] = 'gone@goodbye.com';
		$delete['statusId'] = 2;
		if($this->update($delete)){
			$this->logout();
			return $_SESSION['words'][40]." ".$delete['password'];//Your profile has been deactivated and anonymized. If you'd like to reactivate it, contact us and provide the following code :'
		}else{
			return $_SESSION['words'][11];//there's been a problem
		}
	}

	public function logout() {
		Session::delete($this->_sessionName);
		$this->_isLoggedIn = false;
		$this->_sessionName = null;
		$_SESSION = array();
		helper::loadLanguage();
	}

	public function handle_notif($notificationTypeId,$action){
		switch ($action) {
			case 'add':
				// insert new notification subscription
			$query = $this->_db->query("INSERT INTO notif_subscription SET userId = ?,notificationTypeId = ?",
				array($this->_data->userId,$notificationTypeId));
			break;
			case 'delete':
			$query = $this->_db->query("DELETE FROM notif_subscription WHERE userId = ? and notificationTypeId = ?",
				array($this->_data->userId,$notificationTypeId));
			break;
			default:
			$query = $this->_db->query("SELECT COUNT(*) FROM notif_subscription WHERE userId = ? and notificationTypeId = ?",
				array($this->_data->userId,$notificationTypeId));
			if($query->first()>0){
				return TRUE;
			}
			return FALSE;
			break;
		}
	}

	public function getUserRelatedRecords(){
		$query = $this->_db->query('
			 SELECT "s" as type,c.icon, title, description, "" as proposition, solutionId as itemId,DATE_FORMAT(createdOn,"%d %m %Y") as age,(SELECT round(COUNT(DISTINCT pr.propositionId)) from proposition AS pr where pr.solutionId=s.solutionId and pr.statusId <> 5) AS count, (SELECT COUNT(DISTINCT pr.propositionId) from proposition AS pr where pr.solutionId=s.solutionId and pr.statusId <> 5) AS nbItems FROM solution as s inner join category as c on s.categoryId = c.categoryId WHERE userId = ? and s.statusId <> 5',array($this->_data->userId));
		$results['solutions'] = $query->results();
		$query = $this->_db->query('
		SELECT "p" as type,c.icon, title, description, "" as proposition, problemId as itemId,DATE_FORMAT(p.createdOn,"%d %m %Y") as age, (SELECT round(COUNT(DISTINCT v.voteId)) from vote AS v INNER JOIN facet as f ON f.facetId = v.facetId where f.problemId=p.problemId) AS count,(SELECT COUNT(DISTINCT pe.propositionId) from pertinence AS pe INNER JOIN facet as f ON f.facetId = pe.facetId inner join proposition as pro on pro.propositionId = pe.propositionId where f.problemId = p.problemId and pro.statusId <> 5) AS nbItems FROM problem as p inner join category as c on p.categoryId = c.categoryId WHERE userId = ? and p.statusId <> 5',array($this->_data->userId));
				$results['problems'] = $query->results();
		$query = $this->_db->query('SET @user = ?;',array($this->_data->userId));
		$query = $this->_db->query('
		SELECT "v" as type,c.icon, p.title, f.description, "" as proposition,p.problemId as itemId,DATE_FORMAT(p.createdOn,"%d %m %Y") as age, (SELECT round(COUNT(DISTINCT v.voteId)) from vote AS v INNER JOIN facet as f ON f.facetId = v.facetId where f.problemId=p.problemId ) AS count ,(SELECT COUNT(DISTINCT pe.propositionId) from pertinence AS pe INNER JOIN facet as f ON f.facetId = pe.facetId inner join proposition as pro on pro.propositionId = pe.propositionId where f.problemId = p.problemId and pro.statusId <> 5) AS nbItems FROM facet as f INNER JOIN vote as v on v.facetId = f.facetId INNER JOIN problem as p on p.problemId = f.problemId inner join category as c on p.categoryId = c.categoryId WHERE v.userId = ? and p.statusId <> 5',array($this->_data->userId));
				$results['voted'] = $query->results();
		$query = $this->_db->query('SET @user = ?;',array($this->_data->userId));
		$query = $this->_db->query('
		SELECT "pr" as type,"" as icon, p.title, s.title as description,pr.title as proposition, p.problemId as itemId,DATE_FORMAT(pr.createdOn,"%d %m %Y") as age,  (SELECT round(COUNT(DISTINCT pv.pertinenceVoteId)/COUNT(DISTINCT v.voteId)*100) AS nbVotes FROM pertinence as pe LEFT JOIN pertinencevote as pv on pe.pertinenceId = pv.pertinenceId left JOIN facet as f on f.facetId = pe.facetId INNER JOIN vote as v on v.facetId = f.facetId WHERE pe.positive = 1 and pe.propositionId = pr.propositionId ) AS count, "" as nbItems FROM proposition as pr left JOIN solution as s on pr.solutionId = s.solutionId left JOIN pertinence as pe on pe.propositionId = pr.propositionId left JOIN facet as f on f.facetId = pe.facetId left JOIN problem as p on p.problemId = f.problemId WHERE pr.userId = ? and pr.statusId <> 5 and s.statusId <> 5 group by p.title, s.title, pr.title, pr.propositionId',array($this->_data->userId));
			$results['propositions'] = $query->results();
			// helper::logError($query->debug());
			return $results;
	}
}
