<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class tags{

	private $_db;
  public $_data;

	function __construct($id = null){
  	$this->_db = DB::getInstance();
	}

  public function exists($tag){
    $query = $this->_db->query("SELECT * FROM tag WHERE
      name = ?", array($tag));
    return ($query->count()>0) ? true : false;
  }

  public function add($tags = array()){
    foreach ($tags as $tag) {
      if(!$this->exists($tag)){
        $this->_db->insert('tag',array('name'=>$tag,'language'=>session::get('language')));
      }
    }
    $this->loadList();
  }

  public function loadList($text = null){
    $text = '%'.$text.'%';
    $query = $this->_db->query("SELECT name FROM tag WHERE
      language = ? and  name like ? limit 25", array(session::get('language'),$text));
    $this->_data = $query->results();
    $results = array();
    foreach ( $this->_data as $item) {
        array_push($results,$item->name);
      }
    $jsonfile = json_encode($results);
    return $jsonfile;
  }

}
