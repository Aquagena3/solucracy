<?php
require('core/ini.php');
helper::loadHeader('header.php',array(
		'TITLE'=>'test'
		,'DESCRIPTION'=>$_SESSION['words'][80]));


?>
	<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid full-height marginbt100 m-0">
	<div itemprop="description" style="display: none;">Nous avons tous les mêmes problèmes, trouvons ensemble les solutions</div>
		<div class="row full-height">
			<div class="col-12 full-height m-0 pl-0">
				<div id="mapContainerShow" class="full-height" style="position:relative">
					<div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo Config::get('GMap_API_Key') ?>" width="WIDTH" height="HEIGHT" ></div>
				</div>
			</div>
		</div>
	</div>
<?php
include("inc/footer.php");
?>
<script type="text/javascript">
	$('header').hide();
</script>
