<?php
 require('core/ini.php');
 helper::loadHeader('header.php',array(
		'TITLE'=>'FAQ'
		,'DESCRIPTION'=>'help'));
?>
<div class="container-fluid">
	<div class="offset-3 col-md-6 list w-100 ">
<h1 id="terms">Conditions générales</h1></br>
<h2>Merci de l’intérêt que vous portez à Solucracy.</h2></br>
<h3>Ce site se veut être un site collaboratif et comme pour toute communauté, nous nous devons en tant que membres de respecter certaines règles :</h3></br>
<ul>
 	<li>Merci de ne pas faire de publicité pour vos forums, blogs ou sites s’ils ne sont pas directement liés au problème ou à la conversation en cours.</li>
 	<li>Les administrateurs se réservent le droit d’effacer tout message ou post qu’ils jugeront contraire à la mentalité du site.</li>
 	<li>Merci de vérifier que les problèmes ou solutions que vous postez n’existent pas déjà sur le site à l’aide des outils de recherche que nous vous fournissons.</li>
 	<li>Merci de rester courtois, l’objectif ici est de lister les problèmes pour les régler, pas d’en créer de nouveaux.</li>
 	<li>Ce qui est un problème pour vous ne l’est pas forcément pour quelqu’un d’autre. Vous allez probablement voir des problèmes qui vous semblent insignifiants ou ridicules, voire même offensants, ils sont néanmoins tout à fait réels pour d’autres. Merci de respecter cela.</li>
</ul>
<h3>Qu'allez-vous faire de mes données ?</h3>
Solucracy a besoin de certaines de vos données pour fonctionner correctement :
<h4>Email :</h4>
Si vous avez créé un compte ou si vous vous êtes abonné à la newsletter, Solucracy conservera votre email pour que vous puissiez vous connecter. Il sera également utilisé pour vous envoyer des notifications si vous avez activé cette option.

Lorsqu'une personne souhaite vous inviter sur Solucracy, vous avez le choix de laisser Solucracy conserver votre email pour que le système sache que vous ne souhaitez plus recevoir d'invitations ou de l'effacer ( mais dans ce cas, si 18 de vos amis vous invitent, vous recevrez 18 emails). Dans le futur, les emails seront stockés cryptés si vous ne souhaitez pas recevoir d'invitation et utilisés uniquement pour comparaison pour éviter de vous inviter 15 fois de suite.
<h4>Adresse/position géographique :</h4>
Solucracy utilise l'adresse que vous avez saisie pour placer les différents votes sur la carte. Nous vous encourageons à ne pas mettre votre adresse exacte, le nom de rue suffit. Nous n'affichons pas les votes sur la carte s'il y en a moins de 4 sur le territoire affiché pour éviter que quelqu'un puisse faire le lien entre la position du vote et votre compte si c'est vous qui avez renseigné le problème.
<h3>Délai de récupération/suppression des données</h3>
<h4>Récupération des données :</h4>
Vous pouvez récupérer votre adresse email et votre position géographique en vous rendant sur votre page de profil et en cliquant sur paramètres.

Si vous souhaitez connaitre toutes les infos liées à votre activité présentes dans le système, il suffit de nous contacter en utilisant le formulaire de contact. Le traitement de votre demande peut prendre jusqu'à 1 semaine. Si vous en avez vraiment besoin plus tôt, merci de le mentionner dans l'email et on vous contactera.
<h4>Suppression des données :</h4>
Si vous supprimez votre compte, vos données personnelles seront instantanément supprimées et toute votre activité dans le système sera anonymisée. Un code vous sera transmis et l'unique manière de revenir en arrière sera de nous communiquer ce code, une adresse email et un nom de compte pour que nous puissions rétablir ces données.
<h3>Et ci-dessous, tout un tas de trucs légaux….</h3>
Le présent document a pour objet de définir les modalités et conditions dans lesquelles d’une part Solucracy , ci-après dénommé l’EDITEUR, met à la disposition de ses utilisateurs le site, et les services disponibles sur le site et d’autre part, la manière par laquelle l’utilisateur accède au site et utilise ses services.
Toute connexion au site est subordonnée au respect des présentes conditions.
Pour l’utilisateur, le simple accès au site de l’EDITEUR à l’adresse URL suivante www.solucracy.com implique l’acceptation de l’ensemble des conditions décrites ci-après.
Propriété intellectuelle
Tous les éléments de ce site, y compris les documents téléchargeables, sont libres de droit. A l’exception de l’iconographie, la reproduction des pages de ce site est autorisée à la condition d’y mentionner la source. Elles ne peuvent être utilisées à des fins commerciales et publicitaires.
Liens hypertextes
Le site www.solucracy.com peut contenir des liens hypertextes vers d’autres sites présents sur le réseau Internet. Les liens vers ces autres ressources vous font quitter le site www.solucracy.com
Il est possible de créer un lien vers la page de présentation de ce site sans autorisation expresse de l’EDITEUR. Aucune autorisation ou demande d’information préalable ne peut être exigée par l’éditeur à l’égard d’un site qui souhaite établir un lien vers le site de l’éditeur. Il convient toutefois d’afficher ce site dans une nouvelle fenêtre du navigateur. Cependant, l’EDITEUR se réserve le droit de demander la suppression d’un lien qu’il estime non conforme à l’objet du site www.solucracy.com.
Responsabilité de l’éditeur
Les informations et/ou documents figurant sur ce site et/ou accessibles par ce site proviennent de sources considérées comme étant fiables.
Toutefois, ces informations et/ou documents sont susceptibles de contenir des inexactitudes techniques et des erreurs typographiques.
L’EDITEUR se réserve le droit de les corriger, dès que ces erreurs sont portées à sa connaissance.
Il est fortement recommandé de vérifier l’exactitude et la pertinence des informations et/ou documents mis à disposition sur ce site.
Les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent avoir fait l’objet de mises à jour. En particulier, ils peuvent avoir fait l’objet d’une mise à jour entre le moment de leur téléchargement et celui où l’utilisateur en prend connaissance.
L’utilisation des informations et/ou documents disponibles sur ce site se fait sous l’entière et seule responsabilité de l’utilisateur, qui assume la totalité des conséquences pouvant en découler, sans que l’EDITEUR puisse être recherché à ce titre, et sans recours contre ce dernier.
L’EDITEUR ne pourra en aucun cas être tenu responsable de tout dommage de quelque nature qu’il soit résultant de l’interprétation ou de l’utilisation des informations et/ou documents disponibles sur ce site.
Accès au site
L’éditeur s’efforce de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou d’un événement hors du contrôle de l’EDITEUR, et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site et des services.
Par conséquent, l’EDITEUR ne peut garantir une disponibilité du site et/ou des services, une fiabilité des transmissions et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune assistance technique vis à vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.
La responsabilité de l’éditeur ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou d’utilisation des services.
Par ailleurs, l’EDITEUR peut être amené à interrompre le site ou une partie des services, à tout moment sans préavis, le tout sans droit à indemnités. L’utilisateur reconnaît et accepte que l’EDITEUR ne soit pas responsable des interruptions, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers.
Modification des conditions d’utilisation
L’EDITEUR se réserve la possibilité de modifier, à tout moment et sans préavis, les présentes conditions d’utilisation afin de les adapter aux évolutions du site et/ou de son exploitation.
Règles d'usage d'Internet
L’utilisateur déclare accepter les caractéristiques et les limites d’Internet, et notamment reconnaît que :
L’EDITEUR n’assume aucune responsabilité sur les services accessibles par Internet et n’exerce aucun contrôle de quelque forme que ce soit sur la nature et les caractéristiques des données qui pourraient transiter par l’intermédiaire de son centre serveur.
L’utilisateur reconnaît que les données circulant sur Internet ne sont pas protégées notamment contre les détournements éventuels. La communication de toute information jugée par l’utilisateur de nature sensible ou confidentielle se fait à ses risques et périls.
L’utilisateur reconnaît que les données circulant sur Internet peuvent être réglementées en termes d’usage ou être protégées par un droit de propriété.
L’utilisateur est seul responsable de l’usage des données qu’il consulte, interroge et transfère sur Internet.
L’utilisateur reconnaît que l’EDITEUR ne dispose d’aucun moyen de contrôle sur le contenu des services accessibles sur Internet
Droit applicable
Tant le présent site que les modalités et conditions de son utilisation sont régis par le droit français, quel que soit le lieu d’utilisation. En cas de contestation éventuelle, et après l’échec de toute tentative de recherche d’une solution amiable, les tribunaux français seront seuls compétents pour connaître de ce litige.
Conformément à la loi « informatique et libertés » du 6 janvier 1978 modifiée, vous disposez d’un droit d’accès et de rectification aux informations qui vous concernent.
Vous pouvez accèder aux informations vous concernant en vous adressant à :
<img class="alignnone size-full wp-image-124" src="http://www.solucracy.com/blog/wp-content/uploads/2017/12/email.png" alt="" width="172" height="19" />

Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.
Pour en savoir plus, consultez vos droits sur le site de la CNIL.
Pour toute question relative aux présentes conditions d’utilisation du site, vous pouvez nous écrire à l’adresse suivante : <img class="alignnone size-full wp-image-124" src="http://www.solucracy.com/blog/wp-content/uploads/2017/12/email.png" alt="" width="172" height="19" />
" Les présentes conditions générales (CGU) sont une reproduction intégrale ou partielle du Modèle de CGU (conditions générales d'utilisation) d'un site Internet, dont le titulaire des droits d'auteur est DROITISSIMO.COM, site internet d'information juridique grand public. "</p>

<h1 id="why">Pourquoi enregistrer mes coordonnées ?</h1></br>
L'objectif de Solucracy est de définir combien de personnes sont concernées par des problèmes sur une certaine zone géographique.
Lorsque vous voterez pour un problème, cela ajoutera un point sur la carte du problème.
Ainsi, si ce problème est relatif à votre commune, vous pourrez savoir combien de personnes sur votre commune sont concernées.
Cela permettra aux administrateurs de votre commune d'obtenir une liste des problèmes à régler sur leur juridiction.
<strong>IMPORTANT</strong> :Les autres utilisateurs ne pourront pas connaitre vos coordonnées et nous ne les transmettront à personne.
</div>
</div>
<?php
include("inc/footer.php");
?>
