<?php
 require('core/ini.php');


helper::loadHeader('header.php',array(
		'TITLE'=>'Solucracy'
		,'DESCRIPTION'=>$_SESSION['words'][231]));
$country = 'France';
$badges = badge::topFour($country);
$displayCountryBadges = '';
$displayWorldBadges = '';
$activeProblems = helper::countActive('problem');
$activeSolutions = helper::countActive('solution');
$activeCommunities = helper::countActive('community');
foreach ($badges as $badge) {
	if($badge->scope ==='country'){
		$badge->name .= '('.$country.')';
		$displayCountryBadges .= helper::render('topBadges',$badge);
	}else{
		$displayWorldBadges .= helper::render('topBadges',$badge);
	}
}
$form = new form();

?>
	<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid">
	<div itemprop="description" style="display: none;">Nous avons tous les mêmes problèmes, trouvons ensemble les solutions</div>
		<div class="row">
			<div class="col-12 col-md-8">
				<div class="row d-flex justify-content-around text-center">
						<a href="problems.php" class="card m-1 redBorder font_white col-sm-12 col-md-5">
							<div class="p-2 text-center faded_red_bkgd2">
								<i class="fas fa-3x fa-list-ul"></i>
							</div>
							<div class="card-body p-2 faded_red_bkgd2">
								<h4>Liste des problèmes (<?php echo $activeProblems; ?>)</h4>
							</div>
						</a>
						<a href="solutions.php" class="card m-1 greenBorder font_white col-sm-12 col-md-5">
							<div class="p-2 text-center faded_green_bkgd2">
								<i class="fas fa-3x fa-list-ul"></i>
							</div>
							<div class="card-body p-2 faded_green_bkgd2">
								<h4>Liste des solutions (<?php echo $activeSolutions; ?>)</h4>
							</div>
						</a>
						<a href="#" class="card m-1 redBorder font_white col-sm-12 col-md-5" onclick="ajax('buildform.php',{type:'newProblem'},'form')">
							<div class="p-2 text-center faded_red_bkgd2">
								<i class="fas fa-3x fa-plus"></i>
							</div>
							<div class="card-body p-2 faded_red_bkgd2">
								<h4>Vous avez identifié un problème ?</h4>
							</div>
						</a>
						<a href="#" class="card m-1 greenBorder font_white col-sm-12 col-md-5" onclick="ajax('buildform.php',{type:'newSolution'},'form')">
							<div class="p-2 text-center faded_green_bkgd2">
								<i class="fas fa-3x fa-plus"></i>
							</div>
							<div class="card-body p-2 faded_green_bkgd2">
								<h4>Vous voulez proposer une solution ?</h4>
							</div>
						</a>

						<a href="<?php echo Config::get('donationPage'); ?>" class="card m-1 greenBorder font_white col-sm-12 col-md-5">
							<div class="p-2 text-center faded_green_bkgd2">
								<i class="fas fa-3x fa-euro-sign"></i>
							</div>
							<div class="card-body p-2 faded_green_bkgd2">
								<h4>Apporter votre soutien en faisant un don</h4>
							</div>
						</a>
						<a href="#" class="card m-1 greenBorder font_white col-sm-12 col-md-5" onclick="ajax('buildform.php',{type:'invite'},'form')">
							<div class="p-2 text-center faded_green_bkgd2">
								<i class="far fa-3x fa-envelope"></i>
							</div>
							<div class="card-body p-2 faded_green_bkgd2">
								<h4>Invitez vos amis... votre famille...le monde !</h4>
							</div>
						</a>

					<a href="#" class="card m-1 greenBorder font_white row text-center " onclick="ajax('buildform.php',{type:'getCommunityUrl'},'form')">
						<div class="p-2 faded_green_bkgd2">
							<i class="far fa-3x fa-address-card"></i>
						</div>
						<div class="card-body p-2 faded_green_bkgd2">
							<h4>Regardez si votre commune, association, entreprise a déjà créé un compte.</h4>
						</div>
						<div class="faded_green_bkgd2 muted ">
							Actuellement <?php echo $activeCommunities; ?> comptes communauté ont été créés
						</div>
					</a>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="card-deck">
				<?php
echo $displayWorldBadges;
				?>
				</div>
				<div class="card-deck">
				<?php
echo $displayCountryBadges;
				?>
				</div>
				<div class="list">
					<!-- mettre les posts ici / RSS ? -->
				</div>
			</div>
		</div>
	</div>


<?php
include("inc/footer.php");
?>
