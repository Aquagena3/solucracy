<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');
$data = Input::get('full_array');
$user = new user();
// check if user is logged
if(!$user->isLoggedIn()){
		echo helper::outcome(3,FALSE);
		return;
	}
if(isset($data['code']) && $data['code'] !== ''){
	if($user->checkPhoneCode($data['code'])){
		$user->updateStatus(1);
		$user->addUserRole(Session::get('user'),'verified');
		badge::createUnique(3,Session::get('user'),Session::get('user'));
		echo helper::outcome(31,TRUE);//Your account is now active !
		exit();
	}else{
		echo helper::outcome(32,FALSE);//This code is incorrect
		exit();
	}
} else {
	echo helper::outcome(33,FALSE);//Please enter the validation code sent to your phone
	exit();
}

