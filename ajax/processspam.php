<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');
//vérifier que la personne soit connectée
	$isHelogged = new user();
	if(!$isHelogged->isLoggedIn()){
		echo helper::outcome(3,FALSE);
		return;
	}
	if(!$isHelogged->checkRole('moderator')){
		echo helper::outcome(400,FALSE);
		return;
	}
$data = Input::get('full_array');

$spam = new spam();
$message = $spam->process($data['flaggedBy'],$data['spamId'],$data['outcome'],$data['entity'],$data['entityId']);
echo helper::outcome(15,true);//This spam has been processed !
exit();
