<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');

if(Input::defined('coordinates')){

	$problem = new problem(Input::get('problemId'));
	$coordinates = Input::get('coordinates');
	$votes = $problem->getVoteCoordinates($coordinates);
	if(count($votes)> Config::get('remember/nb_votes')){
		echo helper::outcome($votes,true);
		exit();
	} else {
		echo helper::outcome(64,true);//There aren't enough votes to display in this area
		exit();
	}
}else {
    echo helper::outcome(100,false);//there's something missing...
    exit();
 }

?>
