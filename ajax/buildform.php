<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
	require('../core/ini.php');


		//using the connected userId if there's one
		$user = new user();
		//Getting data to build the form
		$data = Input::get('full_array');
		// helper::outcome($data,FALSE);
		// exit();
		//Log history to know which popup was loaded
		helper::logHistory($data['type']);
		$formContent = '';
		//je regarde quel type de formulaire c'est
			//si c'est un formulaire qui nécessite d'être connecté, je regarde si l'utilisateur est connecté
		// si c'est bon, je continue
		//sinon je renvoie le formulaire pour se connecter
		if(in_array($data['type'],array('vote','newProblem','newProposition','newSolution','spam','votePropositions','login','invite','moderation','solutionVote','userSettings','newCommunity','cancelSubscription','communityChoice','manageAdmins','contactUsers','contactProblemUsers','communitySettings','notMyProblem','supportProposition','selectSolution','userComplete'))){
			if(!$user->isLoggedIn()){
				$data['type'] = 'notConnected';
			}
			//si c'est la page de moderation et qu'il n'est pas moderateur, on empêche l'accès
			if($data['type'] === 'moderation'){
				if(!$user->checkRole('moderator')){
					helper::outcome(118,FALSE);//You don't have access to this page
					exit();
				}
			}
			if(Session::exists('userInfo') && Session::get('userInfo')->statusId != 1 && $data['type'] != 'userComplete'){
				$data['type'] = 'notComplete';
			}
		}
		$form = new form();
		//s'il y a des trucs à ajouter avant et après le formulaire
		$before ="";
		$after = "";
		$header = "";
		$footer = "";
		//suivant le type, je prépare les données
		switch ($data['type']) {
			case 'setNewPassword':
			$db = DB::getInstance();
			if(Input::defined('code')) {
				$result =  $db->query('SELECT * FROM user
				WHERE passwordRequestId = ? AND requestTime < TIME( DATE_SUB( NOW( ) , INTERVAL 24 HOUR ) )',array(Input::get('code')));

				if(!$result->count()>0){
					$title = 173; //your request has expired
					$formId = '';
					$onclick = "$('#formModal').modal('hide')";
					$submitValue = 201;
					$fields = '';
				} else{
					$title = 182;
					$formId = 'newPasswordForm';
					$onclick = "processForm('newPasswordForm','url')";
					$submitValue = 171;
					$fields = '';
					$fields .= $form->createField('hidden','requestId','','',Input::get('code'));
					$fields .= $form->createField('hidden','type','','','setNewPassword');
					$fields .= $form->createField('password','password',45,45);
					$fields .= $form->createField('password','passwordCheck',46,46);
				}
			}
			break;
			case 'notComplete':
			$title = 24; //Please verify your account
			$formId = '';
			$onclick = "resendEmail()";
			$submitValue = 205;
			$fields = '';
			break;
			case 'vote':
				$title = 155; //How are you impacted
				$formId = "newVoteForm";
				$problem = new problem($data['problemId']);
				$facets = $problem->getFacets();
				$fields = '';
				foreach ($facets as $facet) {
					$fields .= '<div class="row"><input id="'.$facet->facetId.'" name="facetId" type="radio" value="'.$facet->facetId.'"><label for="'.$facet->facetId.'">'.$facet->description.'</label></div>';
				}
				$fields .= '<div class="row"><input id="new" name="facetId" type="radio"><label for="new"><input id="newText1" name="newText1" type="text" placeholder="'.$_SESSION['words'][224].'" maxlength="140"></label></div>';//add another reason
				$fields .= $form->createField('swap','newSolution',156,'','data-notificationtypeid="1" data-entityid="'.$data['problemId'].'" onclick="subscribe(this)"');// I would like to be informed of new solutions for this problem
				$fields .= ' <input type="hidden" name="type" id="type" value="vote"/><input type="hidden" name="problemId" id="problemId" value="'.$data['problemId'].'"/>';
				$submitValue = 4;
				$onclick = 'ProcessVote();return false;';
				break;
			case 'forgotPassword':
				$title = 170;
				$formId = 'passwordRequestForm';
				$onclick = "processForm('passwordRequestForm','showAndReload')";
				$submitValue = 171;
				$fields = '';
				$fields .= $form->createField('email','email',44,'');
				$fields .= $form->createField('hidden','type','','','passwordRequest');
				$fields .= $form->createField('captcha','captcha',491,492);
				break;
			case 'selectSolution':
				$formContent .= '<div class="modal-header">
											<h3 class="modal-title text-center">'.$_SESSION['words'][152].' ?</h3><!-- What would you like to do -->
											<button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
										<div class="modal-body d-flex justify-content-around"><a href="solutions.php?problemId='.$data['problemId'].'" class="solucracy_btn padding5">'.$_SESSION['words'][154].'</a><!-- Find an existing solution --><a href="#" class="solucracy_btn padding5" onclick="ajax(\'buildform.php\',{type:\'newSolution\',problemId:\''.$data['problemId'].'\'},\'form\')">'.$_SESSION['words'][244].'</a><!-- Add a solution --></div>';
				echo helper::outcome($formContent,TRUE);
				exit();
				break;
			case 'userComplete':
				$user = new user();
				$check = false;
				$formId = "userCompleteForm";
				$fields = '';
				if($user->get('statusId')==1){
					$title = 468;//Your account has already been activated
					$submitValue = 469;//back to homepage
					$onclick = "window.location.replace('homepage.php');";
				}else{
					// if they are check the salted email against the link
					if(md5($user->get('email') . Config::get('salt')) === Input::get("code")){
						//if it's ok, display the form for the rest
						$title = 142;// Complete your profile
						$check = true;
						$onclick = "processForm('userCompleteForm','showAndRedirect')";
						$fields = '<div class="row"><div class="col-6 col-lg-6">';
						$fields .= $form->createField('hidden','type','','','usercomplete');
						$fields .= $form->createField('hidden','address','','');
						$fields .= $form->createField('hidden','coord','','');
						$fields .= $form->createField('text','location',88,72);
						$fields .= $form->createField('link','why',47,47,'faq'.Session::get('language').'.php#why');
						$fields .= $form->createUnvalidatedField('swap','newProblems',144,'','data-entityid="0" data-notificationtypeid="5" onclick="subscribe(this)"');
						$fields .= '</div><div id="mapContainerEdit" class="col-6 col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="'.Config::get('GMap_API_Key').'" width="378px" height="400px" ></div></div></div>';
						$submitValue = 13;
					} else {
						//if it's not, let them know that they have nothing to do here and send them back to the index
						$title = 384;//The validation link doesn't point to an existing....
						$submitValue = 469;//back to homepage
						$onclick = "window.location.replace('homepage.php');";
					}
				}
				break;
			case 'spam':
				$title = 261; //Je pense que cela devrait être désactivé parce que
				$formId = "spam";
				$fields = '';
				$fields .= $form->createField('text','comment',185,185);
				$fields .= $form->createField('hidden','entity','','',$data['entityType']);
				$fields .= $form->createField('hidden','entityId','','',$data['entityId']);
				$fields .= $form->createField('hidden','type','','','spam');
				$submitValue = 171;
				$onclick = "processForm('spam','showAndReload')";
				break;
			case 'votePropositions':
				$title = 225; //Voici les propositions qui peuvent résoudre votre côté du problème. Pensez-vous qu'elles sont adaptées ?
				$formId = "propositionsForm";
				$problem = new problem($data['problemId']);
				$propositions = $problem->getUserRelevantPropositions();
				$fields = '';
				foreach ($propositions as $prop) {
					//get all the positive pertinences
					$a = array_filter($prop,function($var){ return $var->positive === '1';});
					$positive = array_shift($a);
					$facetId = $positive->facetId;
					$fields .= $form->createField('swap','clauseSlide'.$positive->pertinenceId,$positive->title,$positive->title,'checked');
					$fields .= '<div id="props';
					$fields .= $positive->pertinenceId;
					$fields .= '" style="display: none;" class="offset-2"><p>'.$_SESSION['words'][223].' ?</p>';
					foreach ($prop as $pertinence) {
						if($pertinence->positive === '1'){continue;};
						$fields .= '<div class="row margin5"><input id="per';
						$fields .= $pertinence->pertinenceId;
						$fields .= '" name="prop';
						$fields .= $pertinence->propositionId;
						$fields .= '" type="radio" value="';
						$fields .= $pertinence->pertinenceId;
						$fields .= '"><label for="per';
						$fields .= $pertinence->pertinenceId;
						$fields .= '">';
						$fields .= $pertinence->reason;
						$fields .= '</label></div>';
					}
					$fields .= '<div class="row"><input id="new';
					$fields .= $positive->propositionId;
					$fields .= '" name="prop';
					$fields .= $positive->propositionId;
					$fields .= '" type="radio" class="hidden"><label for="new';
					$fields .= $positive->propositionId;
					$fields .= '"><input id="newText';
					$fields .= $positive->propositionId;
					$fields .= '" name="new';
					$fields .= $positive->propositionId;
					$fields .= '" type="text" placeholder="'.$_SESSION['words'][224].'" maxlength="140"></label></div></div>';
				}
				$fields .= $form->createField('hidden','facetId','','',$facetId);
				$fields .= $form->createField('hidden','type','','','propositions');
				$submitValue = 171;
				$onclick = 'processProps();return false;';
				break;
				case 'notConnected':
				$title = 12; //Connexion
				$formId = "connect";
				$fields = '';
				$fields .= $form->createField('hidden','action','','','login');
				$fields .= $form->createField('email','email',44,44);
				$fields .= $form->createField('password','password',45,45);
				$fields .= '<a name="requestPassword" id="requestPassword" href="#" onclick="ajax(\'buildform.php\',{type:\'forgotPassword\'},\'form\')" class="font_green text-underline"><u>'.$_SESSION['words'][169].'</u></a>';
				$fields .= '</br>';
				$fields .= '<a name="communityAccount" id="communityAccount" href="#" onclick="ajax(\'buildform.php\',{type:\'newAccount\'},\'form\')" class="font_green text-underline"><u>'.$_SESSION['words'][228].'</u></a>';
				$submitValue = 12;
				$onclick = 'login()';
				break;
				case 'newAccount':
				$title = 35; //Participate in Solucracy
				$formId = "newUserForm";
				$fields = '';
				$fields .= $form->createField('text','userName',42,42);
				$fields .= $form->createField('hidden','type','','','newUser');
				$fields .= $form->createField('email','email',44,44);
				$fields .= $form->createField('password','password',45,45);
				$fields .= $form->createField('password','passwordCheck',46,46);
				$fields .= $form->createField('captcha','captcha',491,492);
				$fields .= $form->createField('link','terms_of_use',48,48,'faq'.Session::get('language').'.php#terms');
				$fields .= $form->createField('swap','newsletter',135,135);
				$submitValue = 57;
				$onclick = 'processNewUser()';
				break;
				case 'notifications':
				$title = 385; //Connexion
				$formId = "blank";
				$notifications = notification::getUnread($user->data()->userId);
				$fields = '<div id="notifications" class="list-group">';
				foreach ($notifications as $notif) {
					$fields .= '<a href="#" data-link="';
					$fields .= $notif->link;
					$fields .= '" id="';
					$fields .= $notif->notificationId;
					$fields .= '" class="list-group-item clickable card"><h5 class="list-group-item-heading">';
					$fields .= $notif->title;
					$fields .= '</h5><p class="list-group-item-text">';
					$fields .= $notif->description;
					$fields .= '</p><span class="badge">';
					$fields .= $notif->createdOn;
					$fields .= '</span></a>';
				}
				$fields .= '</div>';
				$submitValue = 201;
				$onclick = '$("#formModal").modal("hide")';
				break;
				case 'invite':
				$title = 41; //Connexion
				$formId = "inviteForm";
				$fields = '';
				$fields .= $form->createField('textarea','emails',114,115);
				$fields .= $form->createField('hidden','type','','','invite');
				$submitValue = 116;
				$onclick = "processForm('inviteForm','showAndReload')";
				break;
				case 'moderation':
				$title = 120; //Page de modération
				$formId = "None";
				$spam = new spam();
				$list = $spam->getList();
				$fields = "";
				foreach ($list as $item) {
					switch ($item->entity) {
						case 'problem':
							$entityName = $_SESSION['words'][310];
							$link = Config::get("base_url").$item->entity.'.php?id='.$item->entityId;
							break;
						case 'solution':
							$entityName = $_SESSION['words'][260];
							$link = Config::get("base_url").$item->entity.'.php?'.$item->entity.'Id='.$item->entityId;
							break;
						case 'proposition':
							$entityName = $_SESSION['words'][311];
							$link = Config::get("base_url").$item->entity.'.php?'.$item->entity.'Id='.$item->entityId;
							break;

						default:
							# code...
							break;
					}
					$fields .= '<div class="row margin5 greenBorder"><div class="col-md-2">'.$entityName.'</div><div class="col-md-2">'.$item->userName.'</div><div class="col-md-2">'.$item->flaggedOn.'</div><div class="col-md-2"><a href="'.$link.'">'.$item->title.'</a></div><div class="col-md-2 p-1">'.$item->comment.'</div><div class="col-md-2"><i class="fa fa-thumbs-up font_green clickable margin5" onclick="processSpam('.$item->flaggedBy.','.$item->spamId.',1,\''.$item->entity.'\','.$item->entityId.')"></i><i class="fa fa-thumbs-down font_red clickable margin5" onclick="processSpam('.$item->flaggedBy.','.$item->spamId.',0,\''.$item->entity.'\','.$item->entityId.')"></i></div></div>';
				}

				$submitValue = 201;
				$onclick = "$('#formModal').modal('hide')";
				break;
				case 'newSolution':
				if(null !== Input::get('problemId') && is_numeric(Input::get('problemId'))){
					Session::put('problemId',Input::get('problemId'));
				}
				$categories = helper::getCategories();
				$title = 244; //AJouter une nouvelle solution
				$formId = "newSolutionForm";
				$fields = "";
				$fields = $form->createField('text','title',1,1);
				$fields .= $form->createField('textarea','description',28,28);
				$fields .= $form->createField('text','solutionTags',2,84);
				$fields .= $form->createField('select','categoryId',85,85,$categories);
				$fields .= $form->createUnvalidatedField('swap','singleProblem',94,94,'checked');
				$fields .= $form->createField('text','avantages',95,95);
				$fields .= $form->createField('hidden','type','','','newSolution');
				$fields .= $form->createField('text','inconvenients',96,96);
				$submitValue = 90;
				$onclick = "processForm('newSolutionForm','newSolution')";
				break;
				case 'solutionVote':
				if($data['voteValue'] == 1){
					$title = 374; //Je vote pour parce que
				} else {
					$title = 375; //Je vote contre parce que
				}

				$formId = "solutionVote";
				$fields = '';
				$fields .= $form->createField('textarea','comment',22,185);
				$fields .= $form->createField('hidden','value','','',$data['voteValue']);
				$fields .= $form->createField('hidden','solutionId','','',$data['solutionId']);
				$fields .= $form->createField('hidden','type','','','solutionVote');
				$submitValue = 57;
				$onclick = "processForm('solutionVote','showAndReload')";
				break;
				case 'userSettings':
				$title = 175; //modifier mon profil
				$formId = "settingsForm";
				$notifications = notification::loadSubscriptions(Session::get('user'));
				$commentReply = (in_array(2, $notifications)) ? 'checked' : "";
				$problemVote = (in_array(4, $notifications)) ? 'checked' : "";
				$problemComment = (in_array(3, $notifications)) ? 'checked' : "";
				$newSolution = (in_array(1, $notifications)) ? 'checked' : "";
				$myTown = (in_array(5, $notifications)) ? 'checked' : "";
				$premiumCity = (in_array(6, $notifications)) ? 'checked' : "";
				$communityContact = (in_array(10, $notifications)) ? 'checked' : "";
				$fields = '<div class="row"><div class="col-6 col-lg-6 text-center">';
				$fields .= $form->createUnvalidatedField('text','userName',42,$user->data()->userName);
				$fields .= $form->createField('hidden','type','','','editUser');
				$fields .= $form->createField('hidden','address','','');
				$fields .= $form->createField('hidden','coord','','');
				$fields .= $form->createUnvalidatedField('email','email',44,$user->data()->email);
				$fields .= $form->createField('password','oldPassword',$_SESSION['words'][45].'<div class="input_caption">'.$_SESSION['words'][381].'</div>',45);
				$fields .= $form->createField('password','newPassword',182,182);
				$fields .= $form->createField('password','settingsPasswordCheck',46,46);
				$fields .= $form->createUnvalidatedField('text','location',125,72);
				$fields .= $form->createUnvalidatedField('swap','delete',372,372,'');
				$fields .='</div><div id="mapContainerEdit" class="col-6 col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="'.Config::get('GMap_API_Key').'" width="378px" height="400px" ></div></div></div>';
				$fields .='<div class="row margin5"><h3>'.$_SESSION['words'][259].'</h3></div>';//I would like to receive an email when...
				$after .= $form->createField('swap','problemVote',257,'',$problemVote.' data-notificationtypeid="4" data-entityid="0" onclick="subscribe(this)"');
				$after .= $form->createField('swap','newSolution',258,'',$newSolution.' data-notificationtypeid="1" data-entityid="0" onclick="subscribe(this)"');
				$after .= $form->createField('swap','MyTown',144,'',$myTown.' data-notificationtypeid="5" data-entityid="0" onclick="subscribe(this)"');
				$after .= $form->createField('swap','comContact',427,'',$communityContact.' data-notificationtypeid="10" data-entityid="0" onclick="subscribe(this)"');
				$submitValue = 179;
				$onclick = 'processChanges()';
				break;
				case 'newProblem':
				$title = 80; //Ajouter un problème
				$formId = "newProblemForm";
				$relatedCommunities = $user->getCommunities();
				$scopeList = helper::scope();
				$categories = helper::getCategories();
				$fields = '<div class="row"><div class="col-6 col-lg-6 text-center">';
				$fields .= $form->createField('text','title',1,1);
				$fields .= $form->createField('textarea','description',28,28);
				$fields .= $form->createField('text','problemTags',2,84);
				$fields .= '<i class="ml-4">'.$_SESSION['words'][84].'</i>';
				$fields .= $form->createField('select','categoryId',85,85,$categories);
				$fields .= $form->createField('text','vote',68,68);
				if(count($relatedCommunities)>0){
					$fields .= $form->createUnvalidatedField('select','communityId',61,61,$relatedCommunities);
					// $fields .= $form->createUnvalidatedField('swap','private',66,66,'');
				}
				$fields .= $form->createField('hidden','type','','','problem');
				$fields .= $form->createField('hidden','address','','');
				$fields .= $form->createField('hidden','coord','','');
				$fields .= $form->createField('text','location',88,72);
				$fields .= $form->createField('select','scopeId',67,67,$scopeList);
				$fields .='</div><div id="mapContainerEdit" class="col-6  col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="'.Config::get('GMap_API_Key').'" width="378px" height="400px" ></div></div></div>';

				$submitValue = 90;
				$onclick = "processForm('newProblemForm','newProblem')";
				break;
				case 'problemShare' :
				$title = 212; //Pourquoi ne pas le partager maintenant ?
				$formId = "ShareProblemForm";
				$problem = new problem(Input::get('id'));
				$problemDetails = $problem->data();
				$fields = '<div itemscope class="row text-center" itemtype="http://schema.org/ItemPage">
					<div class="faded_green_bkgd2 font_white w-100">
						<H5>'.$_SESSION['words'][216].'</H5>
					</div>
				</div>
				<div class="row text-center">
					<div class="topic_pix col-md-2 text-right">
						<img itemprop="image" src="img/'.$problemDetails->categoryIcon.'">
					</div>
					<div class="col-md-9">
							<span itemprop="name">'.$problemDetails->title.'</span></br>
							<span itemprop="description">'.$problemDetails->description.'</span>
					</div>
					<div class="row w-25">
						<div class="col-12 d-flex justify-content-around">
							<a href="http://www.facebook.com/sharer.php?u=www.solucracy.com/problem-'.$problemDetails->problemId.".html&title=".$problemDetails->title.'"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
							<a href="http://reddit.com/submit?url=www.solucracy.com/problem-'.$problemDetails->problemId.'.html&title='.$problemDetails->title.'"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
							<a href="https://twitter.com/intent/tweet?url=www.solucracy.com/problem-'.$problemDetails->problemId.'.html&TEXT='.$problemDetails->title.'"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
							<a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=www.solucracy.com/problem-'.$problemDetails->problemId.'.html"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
						</div>
					</div>
				</div>';
				$submitValue = 201;
				$onclick = "window.location.replace('problem-".$problemDetails->problemId.".html');";
				break;
				case 'solutionShare' :
				$title = 248; //Pourquoi ne pas le partager maintenant ?
				$formId = "ShareSolutionForm";
				$solution = new solution(Input::get('id'));
				$solutionDetails = $solution->data();
				$fields = '<div itemscope class="row text-center" itemtype="http://schema.org/ItemPage">
					<div class="faded_green_bkgd2 font_white w-100">
						<H5>'.$_SESSION['words'][247].'</H5>
					</div>
				</div>
				<div class="row text-center">
					<div class="topic_pix col-md-2 text-right">
						<img itemprop="image" src="img/'.$solutionDetails->categoryIcon.'">
					</div>
					<div class="col-md-9">
							<span itemprop="name">'.$solutionDetails->title.'</span></br>
							<span itemprop="description">'.$solutionDetails->description.'</span>
					</div>
					<div class="row w-25">
						<div class="col-12 d-flex justify-content-around">
							<a href="http://www.facebook.com/sharer.php?u=www.solucracy.com/solution-'.$solutionDetails->solutionId.".html&title=".$solutionDetails->title.'"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
							<a href="http://reddit.com/submit?url=www.solucracy.com/solution-'.$solutionDetails->solutionId.'.html&title='.$solutionDetails->title.'"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
							<a href="https://twitter.com/intent/tweet?url=www.solucracy.com/solution-'.$solutionDetails->solutionId.'.html&TEXT='.$solutionDetails->title.'"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
							<a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=www.solucracy.com/solution-'.$solutionDetails->solutionId.'.html"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
						</div>
					</div>
				</div>';
				$submitValue = 201;
				$onclick = "window.location.replace('solution-".$solutionDetails->solutionId.".html');";
				break;
				case 'newCommunity':
				//renvoyer vers formulaire de contact pour l'instant
				$title = 506; //We're currently looking for a beta site to test the community account, please send us an email if you're interested in this project
				$formId = "contactForm";
				$fields = '';
				$fields .= $form->createField('hidden','type','','','contactEmail');
				$fields .= $form->createField('hidden','address','','');
				$fields .= $form->createField('text','name',137,'');
				$fields .= $form->createField('email','email',44,'');
				$fields .= $form->createField('textarea','emailText',22,22);
				$fields .= $form->createField('captcha','captcha',491,492);
				$submitValue = 171;
				$onclick = "processForm('contactForm','showAndReload')";

				//Une fois que la logique sera un peu plus claire et le service bien défini, décommenter le code ci-dessous
				// 	$title = 386; //Create a community

				// $formId = "newCommunityForm";
				// $communityTypes = helper::getCommunityTypes();
				// $fields = '<div class="ml-4 p-2"><label for="communityTypeId" class="row">'.$_SESSION['words'][387].'</label>
				// 			<div class="row">
				// 			<select type="select" name="communityTypeId" id="communityTypeId" class="validate grayBorder" ><option class="text-muted" value="2" selected>'.$_SESSION['words'][388].'</option><option class="text-muted" value="1"  disabled>'.$_SESSION['words'][389].'</option><option class="text-muted" value="3" disabled>'.$_SESSION['words'][390].'</option></select>
				// 			</div></div>';
				// $fields .= $form->createField('hidden','type','','','newCommunity');
				// $fields .= $form->createField('text','name',$_SESSION['words'][137].' <a id="popUpTitle" style="display:none;" href="#" data-toggle="popover" data-trigger="hover" title="" data-html="true" data-content="<b>'.$_SESSION['words'][486].'</b></br>'.$_SESSION['words'][487].'" width="75" height="75" data-original-title=""><i class="far fa-question-circle font_green"></i></a>',137);
				// $submitValue = 134;
				// $onclick = 'processNewCommunity()';
				break;
				case 'getCommunityUrl':
					$title = 494; //Look for a community amongst the activated accounts
					$name = "";
					if(isset($data['communityName'])){
						$name = 'value="'.$data['communityName'].'"';
					}
				$formId = "findCommunityForm";
				$fields = '';
				$fields .= $form->createField('text','name',137,137);
				$fields .= $form->createField('hidden','type','','','getCommunityUrl');
				$submitValue = 489;
				$onclick = "processForm('findCommunityForm','url')";
				break;
				case 'communityChoice':
					$title = 391; //You are admin for multiple communities please select one

				$formId = "communityChoice";
				$communities = $user->getUserCommunityAdmins();
				$fields = '';
				$fields .= $form->createField('select','communities',392,'',$communities);
				if(Session::exists('communityName')){
					$fields .= $form->createField('submit','user',$_SESSION['words'][466],'','logAsAdmin(-1)');
				}
				$submitValue = 12;
				$onclick = 'logAsAdmin($(\'#communities option:checked\' ).val())';
				break;
				case 'manageAdmins':
				$title = 397; //manage admins
				$formId = "adminManagementForm";
				$community = new community($data['communityId']);
				$admins = $community->getAdmins();
				$fields = '';
				$fields .= '<table id="admins" class="table table-bordered">
										<thead>
											<tr class="table-hov">
												<th></th>
												<th>'.$_SESSION['words'][137].'</th>
												<th>'.$_SESSION['words'][398].'</th>
											</tr>
										</thead>
										<tbody>';
				//afficher les admins existants
				foreach ($admins as $admin) {
					$fields .= '<tr id="admin'.$admin->userId.'">
					<td class="text-center clickable" onclick="deleteAdmin('.$admin->userId.')"><i class="fa fa-trash-alt"></i></td>
					<td>'.$admin->userName.'</td>
					<td>'.$admin->since.'</td>
					</tr>';
				}
				$fields .= '<tr id="newAdmin" class="table-hover">
											<td colspan="3" class="table-success table-hover text-center"><i class="fas fa-plus"></i></td>
										</tr>
											</tbody>
										</table><input type="hidden" name="type" id="type" value="manageAdmins"/><input type="hidden" name="communityId" id="communityId" value="'.$data['communityId'].'"/>';
				$submitValue = 57;
				$onclick = "processForm('adminManagementForm','showAndReload')";
				break;
				case 'contactUsers':
				$title = 406; //manage admins
				$formId = "userContactForm";
				$community = new community($data['communityId']);
				$fields = '';
				$fields .= $form->createField('textarea','text',408,408);
				$fields .= $form->createField('hidden','type','','','contactUsers');
				$fields .= $form->createField('hidden','communityId','','',$data['communityId']);
				$fields .= '
				<div class="row"><input id="list" name="list" type="radio" value="1"><label for="list">'.$_SESSION['words'][409].'</label></div><!--  to all your followers -->
				<div class="row"><input id="inhabitants" name="list" type="radio" value="2"><label for="inhabitants">'.$_SESSION['words'][410].'</label></div> <!-- only to inhabitants -->';
				$submitValue = 171;
				$onclick = "processForm('userContactForm','showAndReload')";
				break;
				case 'contactProblemUsers':
				$title = 413; //contact problem users
				$formId = "problemUsersContactForm";
				$problem = new problem($data['problemId']);
				$fields = '';
				$fields .= $form->createField('textarea','text',407,408);
				$fields .= $form->createField('hidden','type','','','contactProblemUsers');
				$fields .= $form->createField('hidden','problemId','','',$data['problemId']);
				$submitValue = 171;
				$onclick = "processForm('problemUsersContactForm','showAndReload')";
				break;
				case 'newProposition' :
				$problem = new problem(Session::get('problemId'));
				$community = $problem->currentOwner();
				$solution = new solution(Input::get('solutionId'));
				$facets = $problem->getFacets(Input::get('solutionId'));
				$fields = '';
				$displayFacets = "";
				$form = new form();
				foreach ($facets as $facet) {
					$displayFacets .= '<div class="ml-4 p-2"><div class="row">'.$facet->description.'</div><div class="row swap greenBorder"><input type="checkbox" name="facet'.$facet->facetId.'" id="facet'.$facet->facetId.'" class="hidden"><label for="facet'.$facet->facetId.'" class="slider green"></label></div></div>';
				}
				$pertinenceVotes = $problem->getPertinenceVotes('negative');
				if(count($pertinenceVotes)>0){
					$chart = "";
					foreach ($pertinenceVotes as $pertinenceVote) {
						$chart .= ',["'.$pertinenceVote->reason.'", '.$pertinenceVote->nbVotes.']';
					}
				}
				$title = '<div class="row m-1"><h2>'.$_SESSION['words'][505].' :</h2></div> <div class="row m-1"><h3 class="font-italic redBorder">'.$problem->get('title').'</h3></div> <div class="row m-1"> <h2>'.$_SESSION['words'][70].' :</h2></div> <div class="row m-1"><h3  class="font-italic greenBorder">'.$solution->get('title') .'</h3></div>';
				$formId = "newPropositionForm";
				$submitValue = 90;
				$onclick = "processForm('newPropositionForm','url')";
				if($solution->propositionExists(Session::get('problemId'))===0){
					$fields .= $form->createField('hidden','type','','','proposition');
					$fields .= $form->createField('hidden','solutionId','','',$solution->get('solutionId'));
					$fields .= $form->createField('text','title',71,'');
					$fields .= $form->createField('swap','implemented',496,'');

					if(Session::exists('communityAdmin') && $community !== false && $community->communityId === Session::get('communityAdmin')){
						$fields .= $form->createField('swap','support',423,'');
						$fields .= $form->createField('text','comment',185,'');

					}
				} else {
					$title = 73;
					$fields .= $form->createField('hidden','type','','improveProposition');
					$fields .= $form->createField('hidden','propositionId','',$solution->propositionExists(Session::get('problemId')));
				}
				if(count($facets)>0){
				$fields .= '<div class="col-md-6 font_white red"><label for="description">'.$_SESSION['words'][81].' ? <!-- Which facets of the problem does this solve --></label></div>'.$displayFacets;
					if(count($pertinenceVotes)>0){

						$fields .= '<script type="text/javascript">
												google.charts.load(\'current\', {packages: [\'corechart\', \'bar\']});
						google.charts.setOnLoadCallback(drawRightY);

						function drawRightY() {
							var data = google.visualization.arrayToDataTable([
								["Raison", "Nb de votes"]';
						$fields .= 	$chart;
						$fields .= ']);

							var options = {
								chart: {
									subtitle: "';
						$fields .= 	$_SESSION['words'][82];
						$fields .= 	'"//Main reasons why the previous propositions were not pertinent
								},
								title: \'';
						$fields .= 	$_SESSION['words'][87];
						$fields .= '\',
								hAxis: {
									textPosition: \'in\',
									title: \'Nb de votes\',
									minValue: 0,
								},
								vAxis: {
									title: \'Raison\'
								},
								bars: \'horizontal\',
								axes: {
									y: {
										0: {side: \'right\'}
									}
								},
								legend: {position: \'none\'}
							};
							var material = new google.charts.Bar(document.getElementById(\'chart_div\'));
							material.draw(data, options);
						}
						</script>
						<div id="chart_div" style="width: 80%; height: 30%;" ></div>';
					}
				} else {
					$title = 89;
					$submitValue = 91;
					$onclick = "window.location.replace('solutions.php')";
				}
				break;
				case 'communitySettings':
				$title = 480; //Notification settings
				$formId = "communitySettingsForm";
				$notifications = notification::loadCommunitySubscriptions(Session::get('user'),Session::get('communityAdmin'));
				$newProblem = (in_array(5, $notifications)) ? 'checked' : "";
				$newProposition = (in_array(12, $notifications)) ? 'checked' : "";
				$highPertinenceProposition = (in_array(13, $notifications)) ? 'checked' : "";
				$community = new community($data['communityId']);
				$fields = '';
				$before = '<div class="row">';
				$after = '<div class="row margin5">
								<h3>'.$_SESSION['words'][259].'</h3> <!-- I would like to receive an email when -->
							</div>';
				$after .= $form->createField('swap','newProblem',144,'',$newProblem.' data-entityid="'.$data['communityId'].'" data-notificationtypeid="5" onclick="subscribe(this)"');
				$after .= $form->createField('swap','newProposition',415,'',$newProposition.' data-entityid="'.$data['communityId'].'" data-notificationtypeid="12" onclick="subscribe(this)"');
				$after .= $form->createField('swap','highPertinenceProposition',416,'',$highPertinenceProposition.' data-entityid="'.$data['communityId'].'" data-notificationtypeid="13" onclick="subscribe(this)"');
				$after .= '</div>';
				$submitValue = false;
				$onclick = '';
				break;
				case 'notMyProblem':
					$title = 418; //Delegate the problem

				$formId = "notMyProblemForm";
				$fields = '';
				$fields .= $form->createField('hidden','type','','','notMyProblem');
				$fields .= $form->createField('hidden','problemId','','',$data['problemId']);
				$fields .= $form->createField('text','name',137,137);
				$submitValue = 171;
				$onclick = "processForm('notMyProblemForm','showAndReload')";
				break;
				case 'supportProposition':
				$title = 423; //Show your support for this proposition !
				$formId = "supportPropositionForm";
				$fields = '';
				$fields .= $form->createField('hidden','type','','','supportProposition');
				$fields .= $form->createField('hidden','propositionId','','',$data['propositionId']);
				$fields .= $form->createField('text','comment',185,185);
				$submitValue = 171;
				$onclick = "processForm('supportPropositionForm','showAndReload')";
				break;
				case 'cancelSubscription':
				$title = 414; //Show your support for this proposition !
				$formId = "confirmationForm";
				$fields = '';
				$fields .= '<div class="row m-1">'.$_SESSION['words'][456].'</div>';
				$fields .= $form->createField('swap','cancel',457,457);
				$fields .= $form->createField('textarea','text',407,461);
				$submitValue = 414;
				$onclick = 'confirmCancelSubscription()';
				break;
				case 'contactAdmins':
				$title = 139; //Hello, what can we do for you ?
				$formId = "contactForm";
				$fields = '';
				$fields .= $form->createField('hidden','type','','','contactEmail');
				$fields .= $form->createField('hidden','address','','');
				$fields .= $form->createField('text','name',137,'');
				$fields .= $form->createField('email','email',44,'');
				$fields .= $form->createField('textarea','emailText',22,22);
				$fields .= $form->createField('captcha','captcha',491,492);
				$submitValue = 171;
				$onclick = "processForm('contactForm','showAndReload')";
				break;
				case 'newsletterSubscription':
				$email = "";
				if(isset($data['email'])){
					$email = $data['email'];
				}elseif($user->isLoggedIn()){
					$email = $user->get('email');
				}
				$fields = '';
				$fields .= '<div class="modal-header">
											<h3 class="modal-title text-center">'.$_SESSION['words'][222].' :-)</h3><!-- Please enter your email address and click on the button you like the most -->
											<button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
										<div class="modal-body">';
				$fields .= '<form id="subscribeForm" method="post">';
				$fields .= $form->createField('email','email',44,'',$email);
				$fields .= '</div></div></form></div><div class="modal-footer">';
				$fields .= '<div class="row mx-auto p-1">';
				$fields .= $form->createField('submit','subscribe',396,'','newsletterSubscription(true)');
				$fields .= '<div class="m-1"></div>';
				$fields .= $form->createField('submit','subscribe',420,'','newsletterSubscription(false)');
				$fields .= '<div id="alerts" class="ml-0"></div></div>';
				echo helper::outcome($fields,TRUE);
				exit();
			default:
				break;
		}
		//je chope le titre du formulaire et le met en forme
		$title = (is_numeric($title)) ? $_SESSION['words'][$title] : $title;
		$formContent .= '<div class="modal-header">
											<h3 class="modal-title text-center">'.$title.'</h3>
											<button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
		$formContent .= $header;
		$formContent .= '</div>
										<div class="modal-body">';
		//Je met ce qui va avant le formulaire
		$formContent .= $before;
		//je chope les champs du formulaire en fonction du type et je construis avec les petits bouts prédéfinis
		$formContent .= '<form id="'.$formId.'" method="post">'.$fields;

		//mettre un truc pour afficher les messages d'erreur de formulaire
		$formContent .= '</form>';
		//Je met ce qui va après le formulaire
		$formContent .= $after;
		$formContent .='<div class="modal-footer">';
		//s'il y a quelque chose à rajouter au footer
		$formContent .= $footer;
		//je crée le bouton de validation du formulaire
		if($submitValue){
			$formContent .= $form->createField('submit','submit',$_SESSION['words'][$submitValue],'',$onclick);
		}
		$formContent .= '<div id="alerts" class="ml-0"></div></div>';
		//j'envoie tout ça
		echo helper::outcome($formContent,TRUE);
		exit();



