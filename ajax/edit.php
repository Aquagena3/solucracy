<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');

$user = new user();
$data = Input::get('full_array');
// echo helper::outcome($data,FALSE);//uncomment this to test the data received on the page.
// exit();
if(!$user->isLoggedIn()){
	echo helper::outcome(3,FALSE);
	return;
}


$data = Input::get('full_array');
// Input : data + type of data
switch ($data['type']) {
	case 'editUser':
	// validate the data
	if(!$user->checkPassword($data['oldPassword'])){
	echo helper::outcome(180,FALSE);//The current password is incorrect
	exit();
	}
	if(isset($data['delete']) && $data['delete']==='on'){
		if($user->delete()){
			echo helper::outcome('delete',TRUE);
			exit();
		}else{
			echo helper::outcome(11,FALSE);
			exit();
		}
	}
	$validate = new Validate();
	$validation = $validate->check($_POST, array(
		'userName' => array(
			'required' => false,
			'min' => 2,
			'max' => 20,
			'unique' => 'user'),
		'oldPassword' => array(
			'required' => true,
			'min' => 6),
		'newPassword' => array(
			'required' => false,
			'min' => 6),
		'settingsPasswordCheck' => array(
			'required' => false,
			'matches' => 'newPassword'),
		'email' => array(
			'required' => false,
			'valid_email' => true,
			'unique' => 'user')
		));
	if($validation->passed()) {
		$updateData = array();
		if(!empty($data['userName'])){
			$updateData['userName'] = $data['userName'];
		}
		if(!empty($data['coord'])){
			$coordinates = explode(',', trim($data['coord'], '()'));
			$updateData['latitude'] = $coordinates[0];
			$updateData['longitude'] = $coordinates[1];
			$address = (array) json_decode($data['address']);
			$address['latitude'] = $coordinates[0];
			$address['longitude'] = $coordinates[1];
			address::create($address);
		}
		if(!empty($data['newPassword'])){
			$updateData['password'] = md5(helper::test_input($data['newPassword']) . Config::get('salt'));
		}
		if(!empty($data['email'])){
			$updateData['email'] = $data['email'];
		}

		try {
			$result = $user->update($updateData);
		} catch(Exception $e) {
			echo helper::outcome('oh oh problem...',FALSE);
			exit();
		}
		if(!empty($data['email'])){
			$email = new email();
			if($email->sendValidationEmail($data['email'],$user->get('userName'))){
				$user->refreshSession();
				echo helper::outcome(117,TRUE);//You will receive shortly a validation email for your account, please click on the provided link.
				exit();
			} else {
				echo helper::outcome($email->error,FALSE);
				exit();
			}
		} else {
			$user->refreshSession();
			echo helper::outcome(163,TRUE); //Your modifications have been saved.
			exit();
		}

	} else {
		$output = "";
		foreach($validate->errors() as $error) {
			$output = $output.'<br>'.$error;
		}
		echo helper::outcome($output,FALSE);
		exit();
	}
	break;
	default:
	echo helper::outcome(json_encode($data),true);
	break;
}
