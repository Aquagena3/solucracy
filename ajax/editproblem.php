3<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');
$isHelogged = new user();
if(!$isHelogged->isLoggedIn()){
	echo helper::outcome(3,FALSE);
}


$problem = new problem(Input::get('problemId'));

$nbDays = strtotime(date('Y-m-d'))-strtotime($problem->data()->createdOn);
$timePassed = floor($nbDays/(60*60*24));

if(($problem->data()->userId === Session::get(Config::get('session/session_name')) && $timePassed < 14) OR (Input::get('property') === 'description')) {

	$data = array(helper::test_input(Input::get('property'))=>helper::test_input(Input::get('value')));
	$problem->update($data);
	echo helper::outcome(5,TRUE);//Everything is up to date
	exit();
}
	echo helper::outcome(11,false);//There's been a problem
	exit();




