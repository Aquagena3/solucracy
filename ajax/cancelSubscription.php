<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');
//vérifier que la personne soit connectée
	$isHelogged = new user();
	if(!$isHelogged->isLoggedIn()){
		echo helper::outcome(3,FALSE);
		return;
	}
//vérifier que la case soit bien cochée
	if(Input::defined('cancel') && Input::get('cancel') === 'on'){
		//vérifier que la personne soit bien admin de la communauté
		$community = new community(Session::get('communityAdmin'));
		if($community->isAdmin(Session::get('user'))){
			//préparer un code à leur envoyer
			//loguer le code sur le champ communauté et désactiver le compte
			if($community->deactivate()){
				echo helper::outcome(462,TRUE);
				return;
			}
		}else{
			echo helper::outcome(400,FALSE);
		}
	}else{
		echo helper::outcome(458,FALSE);
			return;
	}






