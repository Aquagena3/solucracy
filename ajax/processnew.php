	<?php
	// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
	require('../core/ini.php');

	$isHelogged = new user();

	$data = Input::get('full_array');
	// echo helper::outcome($data,FALSE); //uncomment this to test if the data sent is correct
	// exit();
	if(!in_array($data['type'],array("newUser","getCommunityUrl","contactEmail","reloadCaptcha","passwordRequest","setNewPassword"))){
		if(!$isHelogged->isLoggedIn()){
			echo helper::outcome(3,FALSE);
			return;
		}
	}

	// Input : data + type of data
	switch ($data['type']) {
		case 'passwordRequest':
			$validate = new Validate();
			$validation = $validate->check($data, array(
				'captcha' => array(
					'captcha' => true,
					'required' => true,
					'min' => 6),
				'email' => array(
					'required' => true,
					'valid_email' => true)
				));
			if($validation->passed()) {
				$user = new user();
				if($user->newPasswordRequest($data['email'])){
					echo helper::outcome(167,TRUE);//you're going to receive an email
					exit();
				} else {
					echo helper::outcome(168,FALSE);//We couldn't find anyone with this email
					exit();
				}
			} else {
				$message = "";
				foreach($validate->errors() as $error) {
					$message = $message.'<br>'.$error;
				}
				echo helper::outcome($message,FALSE);
				exit();
			}
		break;
		case 'setNewPassword':
			$validate = new Validate();
			$validation = $validate->check($data, array(
			'requestId' => array(
			'required' => true),
			'password' => array(
				'required' => true,
				'min' => 6),
			'passwordCheck' => array(
				'required' => true,
				'matches' => 'password')
			));
			if($validation->passed()) {
				$password = md5(helper::test_input($data["password"]) . Config::get('salt'));
				$id = Input::get('requestId');
				$db = DB::getInstance();
				$db->query("UPDATE `user` SET `password`=?, passwordRequestId= NULL, requestTime = NULL WHERE passwordRequestId = ?",array($password,$id));
				$output['outcome'] = true;
				$output['message'] = $_SESSION['words'][498];
				$output['url'] = 'homepage.php';
				echo json_encode($output);
				exit();
			}else{
				$output['outcome'] = FALSE;
				$output['message'] = "";
				foreach($validate->errors() as $error) {
					$output['message'] = $output['message'].'<br>'.$error;
				}
				echo json_encode($output);
				exit();
			}
		break;
		case 'sendNewsletter':
		$user = new user();
		if(!$user->checkRole('administrator')){
			echo helper::outcome(400,FALSE);//You don't have the necessary privs to do that
			exit();
		}else{
			$email = new email();
			$list = helper::getNewsletterSubscriptions();
			foreach ($list as $item) {
				$email->sendNewsletter(array('email'=>$item->email,'text'=>$data['text'],'link'=>$data['link']));
			}
			echo helper::outcome("tout a été envoyé !",TRUE);
			exit();
		}
		break;
		case 'reloadCaptcha':
		$file = helper::createCaptcha();
		$fileLocation = Config::get("root_path").'/img/'.$file.'.jpeg';
		echo helper::outcome($fileLocation,TRUE);
		exit();
		break;
		case 'problem':
		if(!$isHelogged->checkRole('verified')){
			echo helper::outcome(24,FALSE);//Please verify your account to be able to do this : Click on your name on the right, then click on settings.
			exit();
		}
			// validate the data
		$validate = new Validate();
		$validation = $validate->check($data, array(
			'title' => array(
				'required' => true,
				'min' => 6),
			'location' => array(
				'required' => true),
			'vote' => array(
				'required' => true,
				'min' => 6),
			'description' => array(
				'required' => true),
			'problemTags' => array(
				'required' => true)
			));

		if($validation->passed()){

			unset($data['location'],$data['type']);
			$problem = new problem();
			if($problem->create($data)){
				$output['outcome'] = TRUE;
				$output['message'] = $_SESSION['words'][56];
				$output['problemId'] = $problem->get('problemId');
				echo json_encode($output);
				//envoyer les notifications aux utilisateurs qui veulent savoir s'il y a un nouveau problème sur leur commune
				$problemDetails = $problem->data();
				$_db = DB::getInstance();
				$_db->query("SELECT u.userId
					from  notif_subscription as ns
					inner join user as u on u.userId = ns.userId
					inner join address as a on u.latitude = a.latitude and u.longitude = a.longitude
					where a.locality = ? and ns.notificationTypeId = 5 group by u.userId order by NULL",array($problemDetails->city));
			//construire ce qu'il faut envoyer comme notifications
				$data['userList'] = $_db->results();
				$data['statusId'] = 7;
				$data['title'] = $_SESSION['words'][16];//A problem has been logged in your city
				$data['notificationTypeId'] = 5;
				$data['description'] = $problemDetails->title;
				$data['link'] = 'problem-'.$problemDetails->problemId.'.html';
				notification::insertList($data);
				notification::sendPending();
			//evaluer si l'utilisateur a droit à un badge
				badge::evaluate('newProblem');
			//créer le newsItem
				newsitem::create(array('newsItemTypeId'=>3,'problemId'=>$problemDetails->problemId));
				exit();
			} else {
				echo helper::outcome(11,FALSE);//Il y a eu un problème
				exit();
			}
		} else {
			$output['outcome'] = FALSE;
			$output['message'] = "";
			foreach($validate->errors() as $error) {
				$output['message'] = $output['message'].'<br>'.$error;
			}
			echo json_encode($output);
			exit();
		}
		break;
		case 'proposition':
		// validate the data
		$checkboxes = array_filter($data,function($var){ return (strpos($var, "facet") === 0);},ARRAY_FILTER_USE_KEY);
		if(empty($checkboxes)){
			echo helper::outcome(17,FALSE);//You need to select at least one facet
			exit();
		}
		$validate = new Validate();
		$validation = $validate->check($data, array(
			'title' => array(
				'required' => true,
				'min' => 6,
				'max' => 200)
			));

		if($validation->passed()) {
			//si ca a été ajouté par une communauté, préparer le lien et le commentaire
			if(isset($data['support'])){
				$comment = $data['comment'];
			}
			//si la proposition est notée comme implémentée
			if(isset($data['implemented'])){
				$data['implemented'] = 1;
			}else{
				$data['implemented'] = 0;
			}
			unset($data['type'],$data['comment']);
			//préparer les pertinences à créer
			$facetIds = "";
			$pertinences = array();
			foreach ($checkboxes as $checkbox => $value) {
				$new = array();
				$new['facetId']  = substr($checkbox, 5);
				$new['positive'] = 1;
				$facetIds.= $new['facetId'];
				end($checkboxes);
				if($checkbox === key($checkboxes)){}else{
					$facetIds.= ",";
				}
				array_push($pertinences, $new);
			}
			//vérifier que la proposition n'existe pas déjà
			if(helper::propositionExists($data['solutionId'],$facetIds)){
				echo helper::outcome(73,FALSE);//This solution and that problem have already been linked.
				exit();
			}
			//créer la proposition
			$proposition = new proposition();
			$message = $proposition->create($data);
			if(isset($comment)){
				$proposition->addCommunityLink(Session::get('communityAdmin'),$comment);
			}
			//créer les pertinences
			foreach ($pertinences as $pertinence) {
				$proposition->addPertinence($pertinence);
			}
			//créer le newsItem
			newsitem::create(array('newsItemTypeId'=>1,'propositionId'=>$message));
			//envoyer les notifications aux utilisateurs qui ont voté pour les facettes
			$_db = DB::getInstance();
			$_db->query("SELECT userId from  vote  where facetId in (".$facetIds.")");
			//construire ce qu'il faut envoyer comme notifications
			$notifications['userList'] = $_db->results();
			$notifications['statusId'] = 7;
			$notifications['title'] = $_SESSION['words'][18];//A solution has been added to one of the problems you voted on
			$notifications['notificationTypeId'] = 1;
			$notifications['description'] = $data['title'];
			$notifications['link'] = 'solution.php?id='.$data['solutionId'];
			$text = notification::insertList($notifications);
			//envoyer les notifications aux utilisateurs qui veulent savoir si une solution a été ajoutée à un problème de leur communauté
			$_db->query("SELECT distinct(ns.userId)
				from notif_subscription as ns
				inner join communityproblem as cp on cp.communityId = ns.entityId
				inner join facet as f on f.problemId = cp.problemId
				where f.facetId in (".$facetIds.") and ns.notificationTypeId = 5");
			//construire ce qu'il faut envoyer comme notifications
			$notifications['userList'] = $_db->results();
			$notifications['title'] = $_SESSION['words'][417];//Il y a une nouvelle solution pour votre communauté
			$notifications['notificationTypeId'] = 5;
			$text = notification::insertList($notifications);
			$output['outcome'] = TRUE;
			$output['url'] = "problem-".Session::get('problemId').".html";
			echo json_encode($output);
			notification::sendPending();
			exit();
		} else {
			$message = "";
			foreach($validate->errors() as $error) {
				$message = $message.'<br>'.$error;
			}
			echo helper::outcome($message,FALSE);
			exit();
		}
		break;
		case 'improveProposition':
			// validate the data
		$checkboxes = array_filter($data,function($var){ return (strpos($var, "facet") === 0);},ARRAY_FILTER_USE_KEY);
		if(empty($checkboxes)){
			$message = $_SESSION['words'][17];//You need to select at least one facet
			echo helper::outcome($message,FALSE);
			exit();
		}
		if($validation->passed()) {
			unset($data['type']);
				//attraper la proposition
			$proposition = new proposition($data['propositionId']);
			$propositionDetails = $proposition->data();
				//créer les pertinences
			$facetIds = "";
			foreach ($checkboxes as $checkbox => $value) {
				$new = array();
				$new['facetId']  = substr($checkbox, 5);
				$new['positive'] = 1;
				$proposition->addPertinence($new);
				$facetIds.= $new['facetId'];
				if(!next( $checkboxes)){}else{
					$facetIds.= ",";
				}
			}
				//envoyer les notifications aux utilisateurs qui ont voté pour les facettes
			$_db = DB::getInstance();
			$_db->query("SELECT userId from  vote  where facetId in (".$facetIds.")");
				//construire ce qu'il faut envoyer comme notifications
			$data['userList'] = $_db->results();
			$data['statusId'] = 7;
			$data['title'] = $_SESSION['words'][16];//A solution has been added to one of the problems you voted on
			$data['notificationTypeId'] = 1;
			$data['description'] = $propositionDetails->title;
			$data['link'] = 'solution.php?id='.$propositionDetails->solutionId;
			notification::insertList($data);
			notification::sendPending();
			//créer le newsItem
			newsitem::create(array('newsItemTypeId'=>1,'propositionId'=>$id));
			echo helper::outcome($data['propositionId'],TRUE);
			exit();


		} else {
			$message = "";
			foreach($validate->errors() as $error) {
				$message = $message.'<br>'.$error;
			}
			echo helper::outcome($message,FALSE);
			exit();
		}
		break;

		case 'newSolution':
			// validate the data
		$validate = new Validate();
		$validation = $validate->check($data, array(
			'title' => array(
				'required' => true,
				'min' => 6,
				'max' => 200),
			'description' => array(
				'required' => true,
				'max' => 600),
			'solutionTags' => array(
				'required' => true),
			'avantages' => array(
				'required' => true,
				'max' => 600),
			'inconvenients' => array(
				'required' => true,
				'max' => 600)
			));

		if($validation->passed()) {
			unset($data['type']);
			$solution = new solution();
			if(isset($data['singleProblem']) && $data['singleProblem'] === 'on'){
				unset($data['singleProblem']);
				$data['oneOff'] = 1;
			}
			$id = $solution->create($data);
			$output['outcome'] = true;
			//créer le newsItem
			newsitem::create(array('newsItemTypeId'=>7,'solutionId'=>$id));
			if(Session::exists('problemId')){
				$output['url'] = 'addproposition.php';
				$output['id'] = $id;
			} else {
				$output['url'] = false;
				$output['id'] = $id;
			}
			echo json_encode($output);
			exit();
		} else {
			$message = "";
			foreach($validate->errors() as $error) {
				$message = $message.'<br>'.$error;
			}
			echo helper::outcome($message,FALSE);
			exit();
		}
		break;
		case 'invite':
			// Check if emails are valid
		$emails = explode(";",$data['emails']);
		$errors = "";
		foreach ($emails as $email) {
			if(!helper::check_email_address($email)){
				$errors .= $email.";";
			}
		}
			//if all email adresses are valid then move forward
		if(empty($errors)){
				//check if people are already in the newsletter list or in the user list, if they are don't send anything
			$newPeople = array_diff($emails,helper::checkEmails($emails));
			if(!empty($newPeople)){
				$email = new email();
				badge::createUnique(3,Session::get('user'),Session::get('user'));
				foreach ($newPeople as $newPerson) {
					if(Session::exists('communityName')){
						if($email->inviteCommunity($newPerson)){
							notification::newsletter($newPerson,0);
						}
					}else{
						if($email->invite($newPerson)){
							notification::newsletter($newPerson,0);
						}
					}
				}
				echo helper::outcome($_SESSION['words'][19]." ".implode(";", $newPeople),true);//Invitations have been sent to the following addresses
				break;
			} else {
				echo helper::outcome(21,false);//All the people mentionned have already received an invitation
				break;
			}

		}else{
			echo helper::outcome($_SESSION['words'][23].$errors,false);//Please enter the following addresses in the right format :
			break;
		}
		case 'propositions':
			// validate the data
		$proposition = new proposition();
		$facetId = $data['facetId'];
			//vérifier que l'utilisateur a bien voté pour la facet quand même
		$problem = new problem();
		if($problem->hasVoted($facetId)){

			// si slide = off : alors si c'est une nouvelle pertinence , on ajoute une pertinence
			$newOnes = array_filter($data, function($key) {
				return strpos($key, 'new')  !== false;
			},ARRAY_FILTER_USE_KEY);
			foreach ($newOnes as $key=>$value) {
				if(!empty($value)){
					$newPertinence['propositionId'] = str_replace('new','',$key);
					$newPertinence['facetId'] = $facetId;
					$newPertinence['reason'] = $value;
					$newPertinence['positive'] = 0;
					$newPertinence['id'] = $proposition->addPertinence($newPertinence);
						// puis on ajoute le vote
					$proposition->addPertinenceVote($newPertinence['id']);
				} else {
					continue;
				}
			}
				//si ce n'est pas une nouvelle, on ajoute juste un vote
			$existingOnes = array_filter($data, function($key) {
				return strpos($key, 'prop')  !== false;
			},ARRAY_FILTER_USE_KEY);
			foreach ($existingOnes as $key=>$value) {
				$proposition->addPertinenceVote($value);
			}
				//si slide = on : alors on ajoute un pertinence vote pour solution adaptée pour la pertinence
			$positives = array_filter($data, function($key) {
				return $key === 'on';
			});

			foreach ($positives as $key=>$value) {
				$pertinenceId = str_replace('clauseSlide','',$key);
				$proposition->addPertinenceVote($pertinenceId);
			}
			echo helper::outcome(25,true); //thank you for your help
			exit();
		}

		break;
		case 'vote':
		if(!$isHelogged->checkRole('verified')){
			echo helper::outcome(24,FALSE);//Please verify your account to be able to vote : Click on your name on the right, then click on settings.
			exit();
		}
		if (Input::defined('facetId')) {
			$problem = new problem($data['problemId']);
			if($data['facetId'] === 'on'){
				if(Input::defined('newText1') && !empty($data['newText1'])){
					$newFacet = array('description' => $data['newText1'],'problemId' => $data['problemId']);
					$facetId = $problem->addFacet($newFacet);
				} else {
					echo helper::outcome(26,FALSE);//Please define how you are impacted by this issue
					exit();
				}
			} else {
				$facetId = $data['facetId'];
			}
			$vote = new vote();
			if(!$vote->alreadyVoted($data['problemId'])){
				if(!$vote->create($facetId)){
					echo helper::outcome(103,FALSE);//Oups, sorry there was a problem processing your vote
					exit();
				}
			} else {
					echo helper::outcome(102,FALSE);//You have already voted for this problem
					exit();
				}
				badge::evaluate('newProblemVote');
				//check if there are any solutions that need to be voted on
				$output['props'] = $problem->countPropsToEvaluate();
				$output['problemId'] = $data['problemId'];
				$output['outcome'] = TRUE;
				$output['message'] = $_SESSION['words'][25]; //Congratulations ! You have voted for a problem !
				echo json_encode($output);
			} else {
				echo helper::outcome(103,FALSE); //Oups, sorry there was a problem processing your vote
				exit();
			}
			break;
			case 'newUser':
			// if something has been submitted, do all the checks
			$validate = new Validate();
			$validation = $validate->check($data, array(
				'userName' => array(
					'required' => false,
					'min' => 2,
					'max' => 20,
					'unique' => 'user'),
				'password' => array(
					'required' => false,
					'min' => 6),
				'captcha' => array(
					'captcha' => true,
					'required' => true,
					'min' => 6),
				'passwordCheck' => array(
					'required' => false,
					'matches' => 'password'),
				'email' => array(
					'required' => false,
					'valid_email' => true,
					'unique' => 'user')
				));
			if($validation->passed()) {
				if(isset($data['newsletter']) && $data['newsletter'] === 'on'){
					notification::newsletter($data['email'],1);
				}
				unset($data['captcha'],$data['type'],$data['newsletter']);
				$user = new user();
				$message = $user->create($data);
				if($message){
					$user->login($data['email'],$data['password']);
					$email = new email();
					$email->sendValidationEmail($data['email'],$data['userName']);
					echo helper::outcome(383,TRUE);//Vous allez recevoir un email de validation, merci de cliquer sur le lien qu'il contient
					exit();
				} else {
					echo helper::outcome(11,FALSE);//there's been a problem
					exit();
				}

				} else {
					$message = "";
					foreach($validate->errors() as $error) {
						$message = $message.'<br>'.$error;
					}
					echo helper::outcome($message,FALSE);
					exit();
				}
				break;
				case 'usercomplete':
				$user = new user();
				// did we get the coordinates from the form ?
				if(Input::defined('coord')){
					//process the coordinates
					$coordinates = explode(',', trim($data['coord'], '()'));
					$address = (array) json_decode($data['address']);
					$address['latitude'] = $coordinates[0];
					$address['longitude'] = $coordinates[1];

					$city = address::create($address);
					//subscribe to notifications

					$communityId = $user->findCommunityId($city);
					if(Input::defined('myTown')){
						notification::subscribe($user->data()->userId,6,$communityId);
					} else {
						notification::unsubscribe($user->data()->userId,6,$communityId);
					}
					if(Input::defined('newProblems')){
						notification::subscribe($user->data()->userId,5,$communityId);
					} else {
						notification::unsubscribe($user->data()->userId,5,$communityId);
					}
					//update user with the phone number, the activation code and the coordinates and add coordinates to the session
					if($user->update(array('latitude'=>$coordinates[0],'longitude'=>$coordinates[1],'communityId' => $communityId,'statusId'=>1))){
						$user->addUserRole(Session::get('user'),'verified');
						badge::createUnique(3,Session::get('user'),Session::get('user'));
						//créer le newsItem
						newsitem::create(array('newsItemTypeId'=>6,'userId'=>$user->data()->userId));
						Session::put('latitude',$coordinates[0]);
						Session::put('longitude',$coordinates[1]);
						$_SESSION['userInfo']->statusId = 1;
					}
					$output['outcome'] = TRUE;
					$output['message'] = $_SESSION['words'][31]; //your account is now active
					$output['url'] = 'homepage.php';
					echo json_encode($output);
					exit();
					} else {
						echo helper::outcome(29,FALSE);//Please enter an address or click on the map
						exit();
					}
				break;
				case 'resendValidation':
					$email = new email();
					$email->sendValidationEmail($isHelogged->get('email'),$isHelogged->get('userName'));
					echo helper::outcome(383,true);
					break;
				case 'newCommunity':
		// validate the data

				if(!isset($data['terms_of_use'])){
					echo helper::outcome(60,false); //please check that you have read the terms of use
				} else {
					unset($data['terms_of_use'],$data['type']);
					$community = new community();
					$result = $community->create($data);
					if($result === 'created'){
						echo helper::outcome($data['name'].' '.$_SESSION['words'][393],true); //has been created and will be activated when the payment has come through
					}elseif($result === 'problem') {
						echo helper::outcome($data['name'].' '.$_SESSION['words'][394],false); // couldn't be created, a problem has occured
					}elseif($result === 'nameNotAvailable'){
						echo helper::outcome($data['name'].' '.$_SESSION['words'][262],false); // couldn't be created, a problem has occured
					}
				}

				break;
				case 'solutionVote':
				$solution = new solution(Input::get('solutionId'));

				if($solution->addVote(helper::test_input(Input::get('value')),helper::test_input(Input::get('comment')))){
					echo helper::outcome(106,true);
				} else {
					echo helper::outcome(103,false);
				};
				break;
				case 'getCommunityUrl':
				$community = new community();
				$community->find('name(departmentId)',$data['name']);
				$output['outcome'] = TRUE;
				$output['url'] = 'communityprofile.php?communityId='.$community->get('communityId');
				echo json_encode($output);
				break;
				case 'spam':
					// validate the data
				$report = new spam();
				$message = $report->report($data['entity'],$data['entityId'],$data['comment']);
				echo helper::outcome($message,true);
				break;
				case 'manageAdmins':
				$community = new community($data['communityId']);
				//Take out the type and the communityId from the array
				unset($data['type'],$data['communityId']);
				//remove duplicates
				$data = array_unique($data);
				//for each email, add the admin and log the results
				$results = "";
				if(count($data)>0){
					foreach ($data as $type => $email) {
						$results .= "</br>".$community->addAdmin($email);
					}
				}else{
					$results = 495; //no new admin has been added
				}

				echo helper::outcome($results,true);
				break;
				case 'contactUsers':
				$community = new community($data['communityId']);
				//prendre la liste des abonnés qui acceptent d'être contactés
				$list = $community->getUserContactList($data['list']);
				//créer la notification pour chacun
				$data['userList'] = $list;
				$data['statusId'] = 7;
				$data['title'] = $_SESSION['words'][411];//A community you subscribed to sent you a message
				$data['notificationTypeId'] = 7;
				$data['description'] = $data['text'];
				$data['link'] = 'communityprofile.php?communityId='.$data['communityId'];
				notification::insertList($data);
				echo helper::outcome(412,true);//Your message has been sent !
				break;
				case 'contactProblemUsers':
				$problem = new problem($data['problemId']);
				//prendre la liste des abonnés qui acceptent d'être contactés
				$list = $problem->getUserContactList();
				//créer la notification pour chacun
				$data['userList'] = $list;
				$data['statusId'] = 7;
				$data['title'] = $_SESSION['words'][411];//A community you subscribed to sent you a message
				$data['notificationTypeId'] = 9;
				$data['description'] = $data['text'];
				$data['link'] = 'problem.php?problemId='.$data['problemId'];
				notification::insertList($data);
				echo helper::outcome(412,true);//Your message has been sent !
				break;
				case 'notMyProblem':
				$community = new community();
				$community->find('name(departmentId)',$data['name']);
				if($community->get('communityId') === Session::get('communityAdmin')){
					echo helper::outcome(502,false);//You can't delegate problems to yourself
					exit();
				}
				$problem = new problem($data['problemId']);
				$problem->addCommunityLink($community->get('communityId'));
				//créer le newsItem
				newsitem::create(array('newsItemTypeId'=>4,'problemId'=>$data['problemId']));
				echo helper::outcome(419,true);//It's not your problem anymore !
				break;

				case 'supportProposition':
				//prendre l'id de la communité qui est loguée
				$proposition = new proposition($data['propositionId']);
				$proposition->addCommunityLink(Session::get('communityAdmin'),$data['comment']);
				//créer le newsItem
				newsitem::create(array('newsItemTypeId'=>9,'propositionId'=>$data['propositionId']));
				//Vérifier si l'auteur de la proposition mérite un badge
				badge::evaluate('propositionSupport',$data['propositionId']);
				echo helper::outcome(424,true);//your support has been recorded
				break;

				case 'contactEmail':
				$validate = new Validate();
				$validation = $validate->check($data, array(
					'captcha' => array(
					'captcha' => true,
					'required' => true,
					'min' => 6),
					'email' => array(
						'required' => true,
						'valid_email' => true),
					'emailText' => array(
					'required' => true)
					));
				if($validation->passed()) {
					$email = new email();
					$email->sendContactEmail(helper::test_input($data["name"]),helper::test_input($data["email"]),helper::test_input($data["emailText"]));
					echo helper::outcome(138,true);//Thank you for your message !
				}else{
					$message = "";
					foreach($validate->errors() as $error) {
						$message = $message.'<br>'.$error;
					}
					echo helper::outcome($message,FALSE);
					exit();
				}
				break;

				default:
				echo helper::outcome(11,false);//There's been a problem
				break;

			}
