<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require('../core/ini.php');
require('../stripe/init.php');
$data = Input::get('full_array');
// echo helper::outcome($_POST,FALSE);//uncomment this to test the data received on the page.
// exit();

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey(Config::get('stripe_API_Key'));
\Stripe\Stripe::setApiVersion("2018-07-27");

// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
$token = $data['stripeToken'];
$amount = $data['amount'].'00';



$charge = \Stripe\Charge::create([
    'amount' => $amount,
    'currency' => 'eur',
    'description' => 'donation de '.$data['stripeEmail'],
    'source' => $token,
]);

$user = new user($data['stripeEmail']);
if($user->exists()){
	$name = $user->get('userName');
}else{
	$name = 'anonyme';
}
helper::newTransaction(array('name'=>$name,'amount'=>substr($amount, 0, -2)));


header('Location: ' .Config::get('base_url'). 'homepage.php?thankYou=""', true,301);
exit();
