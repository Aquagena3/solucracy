<?php
require('core/ini.php');
helper::loadHeader('header.php',array(
		'TITLE'=>'test'
		,'DESCRIPTION'=>$_SESSION['words'][80]));


?>
	<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid full-height marginbt100 m-0">
	<div itemprop="description" style="display: none;">Nous avons tous les mêmes problèmes, trouvons ensemble les solutions</div>
		<div class="row full-height">
			<div itemscope itemtype="http://schema.org/ItemPage" class="col-md-6">
				<div class="row">
					<div class="w-100 d-flex flex-wrap faded_gray_bkgd p-2">
						<h3 class="w-100"><?php echo $_SESSION['words'][161]?></h3><!-- problems -->

					</div>
					<div class="w-100 d-flex justify-content-md-around">
							<div id='start' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='previous' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='current' class="pages font_green" data-nb="0"></div>
							<div id='next' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
							<div id='end' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
					</div>
					<div id="itemList" class="row w-100 list p-3" data-type="problem">
					</div>
				</div>
			</div>
			<div class="col-md-6 col-12 full-height m-0 pl-0">
				<div id="mapContainerShow" class="full-height" style="position:relative">
					<div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo Config::get('GMap_API_Key') ?>" width="WIDTH" height="HEIGHT" ></div>
				</div>
			</div>
		</div>
	</div>
<?php
include("inc/footer.php");
?>

