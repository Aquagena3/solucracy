<?php
require('core/ini.php');
helper::loadHeader('header.php',array(
	'TITLE'=>$_SESSION['words'][127]
	,'DESCRIPTION'=>$_SESSION['words'][127]));
$adminPage = false;
if(Session::exists('communityAdmin') && Input::defined('communityId') && Input::get('communityId') == Session::get('communityAdmin')){
	$adminPage = true;
	$community = new community(Session::get('communityAdmin'));
}elseif(Input::defined('communityId')){
	$community = new community(Input::get('communityId'));
	if($community->get('follower')!=1){
		$follow = '<button id="follow" class="solucracy_btn padding5" onclick="follow(1,'.Input::get('communityId').')">'.$_SESSION['words'][396].'</button><!-- subscribe -->';
	}else{
		$follow = '<button id="follow" class="solucracy_btn padding5" onclick="follow(0,'.Input::get('communityId').')">'.$_SESSION['words'][420].'</button><!-- unsubscribe -->';
	}
}
$problems = $community->getCommunityProblems();
$items = newsitem::getNewsItems('community',$community->get('communityId'));


	//prepare problem list
	$displayProblems = "";
	if($adminPage){
		$displayProblems = helper::displayList('problemListCommunityAdmin',$problems);
	}else{
		$displayProblems = helper::displayList('problemList',$problems);
	}
?>
<div class="container-fluid">
	<div class="row m-2">
		<div class="col-md-8">
			<h3><?php echo $_SESSION['words'][485].' '.$community->get('name'); ?></h3>
<?php
if($adminPage){
?>
<button id="manageAdmins" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'manageAdmins',communityId:<?php echo Session::get('communityAdmin'); ?>},'form')"><?php echo $_SESSION['words'][397]; ?></button><!-- manage admins -->
			<button id="contactUsers" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'contactUsers',communityId:<?php echo Session::get('communityAdmin'); ?>},'form')"><?php echo $_SESSION['words'][406]; ?></button><!-- contact your followers -->
			<button id="communitySettings" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'communitySettings',communityId:<?php echo Session::get('communityAdmin'); ?>},'form')"><?php echo $_SESSION['words'][480]; ?></button><!-- notification settings -->
			<button id="cancel" class="solucracy_btn padding5" onclick="cancelSubscription()"><?php echo $_SESSION['words'][414]; ?></button><!-- notification settings -->
<?php
}else{
	echo $follow;
}
?>

		</div>
		<div class="col-md-4">
			<h3><?php echo $_SESSION['words'][453]; ?></h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 list w-100 ">
			<div class="row d-flex justify-content-md-around">
				<div id='start' class="pages font_green" onclick='displayProblems(this.id)'></div>
				<div id='previous' class="pages font_green" onclick='displayProblems(this.id)'></div>
				<div id='current' class="pages font_green" data-nb="0"></div>
				<div id='next' class="pages font_green" onclick='displayProblems(this.id)'></div>
				<div id='end' class="pages font_green" onclick='displayProblems(this.id)'></div>
			</div>

			<?php
			echo $displayProblems;
			?>
		</div>
		<div class="col-md-4 gray">
				<?php
				foreach ($items as $item) {
		echo helper::render('newsItem',$item);
	}
				?>
		</div>
	</div>
</div>




<?php
include("inc/footer.php");
?>

