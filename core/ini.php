<?php session_start();

// Create a global configuration

$config_file = "config.php";
if (isset($_ENV["CONFIG_FILE"])) {
	$config_file = "config-dev.php";
}
require($config_file);

// Autoload classes
function autoload($class) {

	// Don't interfere with Swift autoloader
if (0 === strpos($class, 'Swift_')) {
		return;
}

	require_once $GLOBALS['config']['root_path']."classes/" . $class . '.php';
}
spl_autoload_register('autoload');
helper::loadlanguage();

if(!Session::exists('userId')){
	Session::put('userId',0);
}
